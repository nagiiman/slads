//Import the express and url modules
  var express = require('express');
  var session = require('express-session');
  var bodyParser = require('body-parser');
  var cookieParser = require('cookie-parser');
  var http = require('http').Server(app);

//Import the mysql module
  var mysql = require('mysql');

//The express module is a function. When it is executed it returns an app object
  var app = express();


app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
  cookieName: 'session',
  secret: 'slads',
  resave: false,
  saveUninitialized: true,
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  httpOnly: true
}));


//Set up express to serve static files from the directory called 'public'
  app.use(express.static('public'));

//Start the app listening on port 8080
  app.listen(8080);

var loginAttempts = 0;
var currentLogin = "";
var events = ["not updated"];
var slaDetails = {total:0, trained:0, senior:0, withdrawn:0, returning:0};
var slaSchool = {ST: 0, HE: 0, AD: 0, LW: 0, BS: 0, MP: 0};
var currPage = "";
var searchResults = [];

app.get('/check', function (req, res) {
  //Create a connection object with the user details
    var con = mysql.createConnection({
      host: "localhost",
      user: "krys17",
      password: "Kristen",
      database: "slads",
	  multipleStatements: true
    });
  //Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors

      //Output results
        console.log("Connected!");
      }
  );
  console.log("loginAttempts: " + loginAttempts)
  console.log("Current Page: " + currPage);
	getEvents();
	getCountFromDB();
	getSeniorCount();
	getWithdrawnCount();
	getReturningSLACount();
	getTrainedSLACount();
	getSLASchoolFromDB();
    if (req.session && req.session.user) {
      var username = req.session.user;
      console.log("USER: " + req.session.user);
      //Build SQL query
        var sql = "SELECT * FROM staff WHERE networkID='" + username + "'; SELECT * FROM student WHERE uniEmail='" + username + "'; SELECT * FROM lecturer WHERE uniEmail='" + username + "'";
    
      //Execute the query
        con.query(sql, function (err, result) {

        //Check for errors
          if (err) throw err;
		  
		  
		  var user = [];
		if(result[0].length > 0) {
			user = result[0];
		} else if(result[1].length > 0) {
			user = result[1];
			user[0].networkID = result[1][0].uniEmail;
		} else if(result[2].length > 0) {
			user = result[2];
			user[0].networkID = result[2][0].uniEmail;
		}
		
            console.log("session running...");
            var staffCheck = user;
            staffCheck.unshift("/check");
			staffCheck[2] = events;
			staffCheck[3] = slaDetails;
			staffCheck[4] = currPage;
			staffCheck[5] = slaSchool;
            //console.log(staffCheck);
            res.send(JSON.stringify(staffCheck));
      
        });
    }
    else {
      console.log("session undefined...");
      res.send("undefined" + loginAttempts);
	  loginAttempts = 0;
    }
    con.end();
});

function getEvents() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT * FROM events";
    
//Execute the query
  con.query(sql, function (err, result) {

    //Check for errors
      if (err) throw err;
	  
	  events = result;
	  //console.log(events);
  });
	con.end();	
}


app.get('/login/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var username = req.query.username;
  var password = req.query.password;
  var dateTime = req.query.clogintime;
  
  if(username == "") {
	  loginAttempts = 21;
  } else if(password == "" || password.length < 3) {
	  loginAttempts = 16;
  }
  else {
//Build SQL query
  var sql = "SELECT * FROM staff WHERE networkID='" + username + "'; SELECT * FROM student WHERE uniEmail='" + username + "'; SELECT * FROM lecturer WHERE uniEmail='" + username + "'";
    
//Execute the query
  con.query(sql, function (err, result) {

    //Check for errors
      if (err) throw err;
        
		var user = [];
		if(result[0].length > 0) {
			user = result[0];
			currPage = "dashboard/staff"
		} else if(result[1].length > 0) {
			user = result[1];
			user[0].networkID = result[1][0].uniEmail;
			if(user[0].role == "Student") {
				currPage = "dashboard/student";
			} else {
				currPage = "dashboard/sla";
			}
		} else if(result[2].length > 0) {
			user = result[2];
			user[0].networkID = result[2][0].uniEmail;
			currPage = "dashboard/lecturer";
		}
		
		//console.log(JSON.stringify(user));
		
		
      var staffCheck = user;
      staffCheck.unshift("/login");
	  
      //console.log(staffCheck);
	  if(staffCheck.length < 2) {
		  loginAttempts++;
		  res.send('login failed' + loginAttempts);
		  console.log('login failed! (length)');
	  }
      else if (staffCheck[1].networkID == "" || staffCheck[1].networkID == null) {
		  loginAttempts++;
        res.send('login failed' + loginAttempts);
		console.log('login failed! (blank)');
      } 
      else if(staffCheck[1].password === password) {
		  loginAttempts = 0;
        req.session.user = staffCheck[1].networkID;
		req.session.userName = staffCheck[1].firstName + ' ' + staffCheck[1].lastName;
        req.session.admin = true;
        console.log(req.session.userName);
		//console.log(JSON.stringify(staffCheck));
        res.send(JSON.stringify(staffCheck));
		
		setTime(staffCheck[1].networkID, staffCheck[1].role, dateTime);
		currentLogin = dateTime;
		
      } else { //passwords don't match
		  loginAttempts++;
        res.send('login failed' + loginAttempts);
		console.log('login failed2! (blank)');
	  }
      
  });
    con.end();
  }
});

function setTime(user, role, time) {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "";
  
  if(role == "Admin" || role == "PAL" || role == "SSLA" || role == "SSLA+") {
	sql = "UPDATE staff SET currentLogin='" + time + "' WHERE networkID='" + user + "'";
  } else {
	sql = "UPDATE " + role.toLowerCase() + " SET currentLogin='" + time + "' WHERE uniEmail='" + user + "'";
  }
    
//Execute the query
  con.query(sql, function (err, result) {

    //Check for errors
      if (err) throw err;
	  
	  console.log("updated current login time.");
  });
	con.end();	
}


app.get('/users/*', handleGetRequest);//Subfolders
app.get('/users', handleGetRequest);


app.get('/search/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var searchBy = req.query.searchBy;
  var searchCriteria = req.query.search;
  console.log(searchCriteria);
  
  if(searchBy == "all") {
  //Build SQL query
    var sql = "SELECT * FROM sla INNER JOIN education ON education.studentNumber=sla.studentNumber INNER JOIN profile ON profile.studentNumber=sla.studentNumber INNER JOIN skills ON skills.studentNumber=sla.studentNumber INNER JOIN reference ON reference.studentNumber=sla.studentNumber ORDER BY sla.firstName";
	
	 con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 searchResults = result;
			 currPage = 'search/sla';
          
	});
	con.end();
	
	
  } else {
		//Build SQL query
		var sql = "SELECT * FROM sla INNER JOIN education ON education.studentNumber=sla.studentNumber INNER JOIN profile ON profile.studentNumber=sla.studentNumber INNER JOIN skills ON skills.studentNumber=sla.studentNumber INNER JOIN reference ON reference.studentNumber=sla.studentNumber WHERE sla." + searchBy +" like '%" + searchCriteria + "%' ORDER BY sla.firstName";
  
    
		//Execute the query
		con.query(sql, function (err, result) {
			//Check for errors
			if (err) throw err;
			//console.log(result);
			//res.send(JSON.stringify(result));
			//searchResults = result;
			currPage = 'search/sla';
			if(result.length > 0) {
				var sql2 = "SELECT * FROM eventassignment INNER JOIN events ON eventassignment.eventID=events.eventID WHERE eventassignment.studentNumber='" + result[0].studentNumber + "'; SELECT * FROM moduleassignment INNER JOIN modules ON moduleassignment.crn=modules.crn WHERE moduleassignment.studentNumber='" + result[0].studentNumber + "'; SELECT * FROM maharaassignment INNER JOIN mahara ON maharaassignment.maharaID=mahara.maharaID WHERE maharaassignment.studentNumber='" + result[0].studentNumber + "'; SELECT * FROM maharacomments INNER JOIN maharaassignment ON maharacomments.maharaAssignmentID=maharaassignment.maharaAssignmentID INNER JOIN mahara ON mahara.maharaID = maharaassignment.maharaID WHERE maharaassignment.studentNumber='" + result[0].studentNumber + "'";
			 
				//Execute the query
				con.query(sql2, function (err, result2) {
				//Check for errors
				if (err) throw err;
				//console.log("events for SLA:" + JSON.stringify(result2));
				result[0].slaEvents = result2[0];
				result[0].slaModules = result2[1];
				result[0].maharaAssignment = result2[2]
				result[0].maharaComments = result2[3];
				searchResults = result;
				//res.send(JSON.stringify(result));
				res.send(JSON.stringify(result));
				console.log(JSON.stringify(result));
				console.log("I sent the events!");
				currPage = 'search/sla';
				
				});
				con.end();
				//res.send(JSON.stringify(result));
			} else {  
				res.send(JSON.stringify(result));
				console.log("didnt send the events...");
			}
		});
	}
});

app.get('/searchModule/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var searchBy = req.query.searchBy;
  var searchCriteria = req.query.search;
  console.log(searchCriteria);
  
  if(searchBy == "all") {
  //Build SQL query
    var sql = "SELECT * FROM modules ORDER BY modules.moduleCode";
	
	 con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 searchResults = result;
			 currPage = 'search/module';
          
	});
	con.end();
	
	
  } else {
		//Build SQL query
		var sql = "SELECT * FROM modules WHERE modules." + searchBy +" like '%" + searchCriteria + "%' ORDER BY modules.moduleCode";
  
    
		//Execute the query
		con.query(sql, function (err, result) {
			//Check for errors
			if (err) throw err;
			//console.log(result);
			//res.send(JSON.stringify(result));
			//searchResults = result;
			currPage = 'search/module';
			if(result.length > 0) {
				var sql2 = "SELECT * FROM moduleassignment INNER JOIN sla ON moduleassignment.studentNumber=sla.studentNumber WHERE moduleassignment.crn='" + result[0].crn + "'";
			 
				//Execute the query
				con.query(sql2, function (err, result2) {
				//Check for errors
				if (err) throw err;
				//console.log("events for SLA:" + JSON.stringify(result2));
				result[0].moduleSLAs = result2;
				searchResults = result;
				//res.send(JSON.stringify(result));
				res.send(JSON.stringify(result));
				console.log(JSON.stringify(result));
				currPage = 'search/module';
				
				});
				con.end();
				//res.send(JSON.stringify(result));
			} else {  
				res.send(JSON.stringify(result));
			}
		});
	}
});





app.get('/searchEvent/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var searchBy = req.query.searchBy;
  var searchCriteria = req.query.search;
  console.log(searchCriteria);
  
  if(searchBy == "all") {
  //Build SQL query
    var sql = "SELECT * FROM events ORDER BY events.date";
	
	 con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 searchResults = result;
			 currPage = 'search/event';
          
	});
	con.end();
	
	
  } else {
		//Build SQL query
		var sql = "SELECT * FROM events WHERE events." + searchBy;
		
		
		if(searchBy == "date") {
			sql += "";
		} else {
			sql += " like '%" + searchCriteria + "%' ORDER BY events.date";
		}
  
    
		//Execute the query
		con.query(sql, function (err, result) {
			//Check for errors
			if (err) throw err;
			//console.log(result);
			//res.send(JSON.stringify(result));
			//searchResults = result;
			currPage = 'search/event';
			if(result.length > 0) {
				var sql2 = "SELECT * FROM eventassignment INNER JOIN sla ON eventassignment.studentNumber=sla.studentNumber WHERE eventassignment.eventID='" + result[0].eventID + "'";
			 
				//Execute the query
				con.query(sql2, function (err, result2) {
				//Check for errors
				if (err) throw err;
				//console.log("events for SLA:" + JSON.stringify(result2));
				result[0].eventSLAs = result2;
				searchResults = result;
				//res.send(JSON.stringify(result));
				res.send(JSON.stringify(result));
				console.log(JSON.stringify(result));
				currPage = 'search/event';
				
				});
				con.end();
				//res.send(JSON.stringify(result));
			} else {  
				res.send(JSON.stringify(result));
			}
		});
	}
});


app.get('/get/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
    var sql = "SELECT * FROM modules ORDER BY modules.moduleCode";
	
	 con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 searchResults = result;
	});
	con.end();

});

app.get('/add/*', function(req, res) {
	
	var pathEnd = req.query.page;

//If path ends with 'users' we return all users
  if(pathEnd === 'sla'){
	currPage = "add/sla";
	res.send('sla');
  } else if(pathEnd === 'event'){
	currPage = "add/event";
	res.send('event');
  } else if(pathEnd === 'general'){
	currPage = "add";
	res.send('add');
  } else if(pathEnd === 'module'){
	currPage = "add/module";
	res.send('module');
  } else if(pathEnd === 'staff'){
	currPage = "add/staff";
	res.send('staff');
  } else if(pathEnd === 'lecturer'){
	currPage = "add/lecturer";
	res.send('lecturer');
  } else if(pathEnd === 'student'){
    currPage = "add/student";
    res.send('student');
    }
});


app.get('/nomination/*', function(req, res) {
	
	var pathEnd = req.query.page;
	
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  var sql = "";
		sql = "SELECT * FROM lecturer INNER JOIN nomination ON lecturer.lecturerID=nomination.lecturerID WHERE lecturer.uniEmail='" + req.session.user + "'; SELECT * FROM nominationmodule WHERE nominationmodule.nominationID=" + "(SELECT nomination.nominationID FROM nomination INNER JOIN lecturer ON lecturer.lecturerID=nomination.lecturerID WHERE lecturer.uniEmail='" + req.session.user + "')";
		
		//Execute the query
		con.query(sql, function (err, result) {
		//console.log("RESULT: " + result);
		
		var sql2 = "";
		for(var a=0; a<result[1].length; a++) {
		sql2 += "SELECT * FROM nominationstudent INNER JOIN student ON student.studentNumber=nominationstudent.studentNumber WHERE nominationstudent.nominationModuleID='" + result[1][a].nominationModuleID + "'; ";
	  }
		//Execute the query
  con.query(sql2, function (err, result2) {
	  
	  if (err) throw err;
	  
		//console.log("RESULT2: " + result2);
		for(var b=0; b<result[1].length; b++) {
			result[1][b].students = result2[b];
		}
    //Check for errors
		if(result[0][0].status == "One") {
			currPage = "nomination/one";
			res.send("nomination/one || " + JSON.stringify(result[0][0]));
		} else if(result[0][0].status == "Two") {
			currPage = "nomination/two";
			res.send("nomination/two || " + JSON.stringify(result[0][0]));
		} else if(result[0][0].status == "Complete") {
			currPage = "nomination/three";
			res.send("nomination/three || " + JSON.stringify(result[0][0]) + " || " + JSON.stringify(result[1]));
		}
		
  });
    con.end();	
  });
    
});

app.post('/nominationLecturer', function (req, res) {
  //Create a connection object with the user details
    var con = mysql.createConnection({
      host: "localhost",
      user: "krys17",
      password: "Kristen",
      database: "slads"
    });
  //Connect to the database
    con.connect(
      //This function is called when the connection is attempted
      function(err) {
        if (err) throw err;//Check for errors
        //Output results
          console.log("Connected!");
        }
    );
      
  //assign staff details
    //var update = req.body;
    console.log(req.body);
   var sql = "UPDATE nomination SET lecturerRole ='" + req.body.lecturerRole + "', expectation ='" + req.body.expectation + "', supportingYear ='" + req.body.supportingYear + "', startTime ='" + req.body.startTime + "', slaFrequency ='" + req.body.slaFrequency + "', status='Two' WHERE lecturerID=" + req.body.lecturerID + "";

  //Execute the query
    con.query(sql, function (err, result) {
      //Check for errors
      if (err) throw err;
          console.log(result)
			res.send("record added!");
			
    });
    con.end();
  });
  

app.post('/nominationModule', function (req, res) {
	for(var m=0; m<req.body.numberOfModules; m++) { 
  //Create a connection object with the user details
    var con = mysql.createConnection({
      host: "localhost",
      user: "krys17",
      password: "Kristen",
      database: "slads",
	  multipleStatements: true
    });
  //Connect to the database
    con.connect(
      //This function is called when the connection is attempted
      function(err) {
        if (err) throw err;//Check for errors
        //Output results
          console.log("Connected!");
        }
    );
      //console.log(req.body);

		console.log(m + ": " + req.body.modules[m]);
    //console.log(req.body);
   var sql = "INSERT INTO nominationmodule (nominationID, type, slaRequirements, moduleCode, moduleTitle, learningEnvironment, comments, observationHelp) VALUES (" + req.body.nominationID + ",'" + req.body.type + "','" + req.body.modules[m].slaRequirements + "','" + req.body.modules[m].moduleCode + "','" + req.body.modules[m].moduleTitle + "','" + req.body.modules[m].learningEnvironment + "','" + req.body.modules[m].comments + "','" + req.body.observationHelp + "')";

  //Execute the query
    con.query(sql, function (err, result) {
      //Check for errors
      if (err) throw err;
        //console.log(result);
		var sql2 = "SELECT * FROM nominationmodule WHERE nominationID=" + req.body.nominationID;
		console.log(req.body.nominationID);
		//Execute the query
		con.query(sql2, function (err, result2) {
		//Check for errors
		if (err) throw err;
        //console.log(result);
		m-=1;
		console.log(result2[m].nominationModuleID);
		for(var s=0; s<req.body.modules[m].students.length; s++) {
			sendStudentToNomination(req.body.nominationID, result2[m].nominationModuleID, req.body.modules[m].students[s].studentNumber, req.body.modules[m].students[s].firstName, req.body.modules[m].students[s].lastName, req.body.modules[m].students[s].uniEmail);
		}
		
		});
		
    });
	//con.end();
}// end of for
	//con.end();
  });

function sendStudentToNomination(nomID, nomMod, studentNumber, fName, lName, uniEmail) {
	 //Create a connection object with the user details
    var con = mysql.createConnection({
      host: "localhost",
      user: "krys17",
      password: "Kristen",
      database: "slads",
	  multipleStatements: true
    });
  //Connect to the database
    con.connect(
      //This function is called when the connection is attempted
      function(err) {
        if (err) throw err;//Check for errors
        //Output results
          console.log("Connected!");
        }
    );
	
	var sql3 = "INSERT INTO nominationstudent (nominationModuleID, studentNumber) VALUES ('" + nomMod + "','" + studentNumber + "'); INSERT IGNORE INTO student (studentNumber, firstName, lastName, uniEmail) VALUES ('" + studentNumber + "','" + fName + "','" + lName + "','" + uniEmail + "'); UPDATE nomination SET status='Complete' WHERE nominationID=" + nomID;

			//Execute the query
			con.query(sql3, function (err, result3) {
				//Check for errors
				if (err) throw err;
				//console.log(result);
			});
	con.end();
}


app.post('/addlecturer', function (req, res) {
  //Create a connection object with the user details
    var con = mysql.createConnection({
      host: "localhost",
      user: "krys17",
      password: "Kristen",
      database: "slads",
	  multipleStatements: true
    });
  //Connect to the database
    con.connect(
      //This function is called when the connection is attempted
      function(err) {
        if (err) throw err;//Check for errors
        //Output results
          console.log("Connected!");
        }
    );
      
  //assign staff details
    //var update = req.body;
    console.log(req.body);
   var sql = "INSERT INTO lecturer (title, firstName, lastName, uniEmail, areaOfStudy, password) VALUES ('"+ req.body.title + "', '" + req.body.firstName + "', '" + req.body.lastName + "', '" + req.body.uniEmail +"', '" + req.body.areaOfStudy + "', '" + req.body.encPW + "')";  

    sendEmail(req.body.password, req.body.firstName, req.body.uniEmail);
  //Execute the query
    con.query(sql, function (err, result) {
      //Check for errors
      if (err) throw err;
        var sql2 = "SELECT * FROM lecturer WHERE uniEmail='" + req.body.uniEmail + "'";

  //Execute the query
    con.query(sql2, function (err, result2) {
      //Check for errors
      if (err) throw err;
        var sql3 = "INSERT INTO nomination (lecturerID, status) VALUES ('" + result2[0].lecturerID + "', 'One')";  
  //Execute the query
    con.query(sql3, function (err, result3) {
      //Check for errors
      if (err) throw err;
          console.log(result)
        
    });
    con.end();
		
    });
		
    });
	
  });
  
  app.post('/addstudent', function (req, res) {
    //Create a connection object with the user details
      var con = mysql.createConnection({
        host: "localhost",
        user: "krys17",
        password: "Kristen",
        database: "slads",
      multipleStatements: true
      });
    //Connect to the database
      con.connect(
        //This function is called when the connection is attempted
        function(err) {
          if (err) throw err;//Check for errors
          //Output results
            console.log("Connected!");
          }
      );
        
    //assign staff details
      //var update = req.body;
      console.log(req.body);
     var sql = "INSERT INTO student (firstName, lastName, uniEmail, studentNumber, password) VALUES ('" + req.body.firstName + "', '" + req.body.lastName + "', '" + req.body.uniEmail +"', '" + req.body.studentNumber + "', '" + req.body.encPW + "')";  
  
      sendEmail(req.body.password, req.body.firstName, req.body.uniEmail);
    //Execute the query
      con.query(sql, function (err, result) {
        //Check for errors
        if (err) throw err;
          var sql2 = "SELECT * FROM student WHERE uniEmail='" + req.body.uniEmail + "'";
  
    //Execute the query
      con.query(sql2, function (err, result2) {
        //Check for errors
        if (err) throw err;
          var sql3 = "INSERT INTO applications (studentNumber, status) VALUES ('" + result2[0].studentNumber + "', 'AppStarted')";  
    //Execute the query
      con.query(sql3, function (err, result3) {
        //Check for errors
        if (err) throw err;
            console.log(result)
          
      });
      con.end();
      
      });
      
      });
    
    });
  



app.post('/nominateStudent', function (req, res) {
  //Create a connection object with the user details
    var con = mysql.createConnection({
      host: "localhost",
      user: "krys17",
      password: "Kristen",
      database: "slads"
    });
  //Connect to the database
    con.connect(
      //This function is called when the connection is attempted
      function(err) {
        if (err) throw err;//Check for errors
        //Output results
          console.log("Connected!");
        }
    );
      
  //assign staff details
    //var update = req.body;
    console.log(req.body);
   var sql = "INSERT INTO student (studentNumber, firstName, lastName, uniEmail, password, status) VALUES ('"+ req.body.studentNumber + "', '" + req.body.firstName + "', '" + req.body.lastName + "', '" + req.body.uniEmail +"', '" + req.body.encPW + "', 'Nominated')";  

    sendEmail(req.body.password, req.body.firstName, req.body.uniEmail);
  //Execute the query
    con.query(sql, function (err, result) {
      //Check for errors
      if (err) throw err;
          console.log(result)
        
    });
    con.end();
  });  


function sendEmail(password, firstName, uniEmail) {
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey("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");
  const msg = {
    to: uniEmail,
    from: 'slads.middlesex@gmail.com',
    //sender_address: 'slads.middlesex@gmail.com',
    //sender_city: 'London',
    //sender_zip: 'NW4 4BT',
    //sender_country: 'United Kingdom',
    subject: 'Welcome to SLADS!',
    text: 'and easy to do anywhere, even with Node.js',
    html: '<strong>Hi ' + firstName +',</strong> <br> <p>I hope this email finds you well. <br> Your SLADS credentials are as follows: <br> Username : '+ uniEmail +' <br> Password: '+ password +' <br> Thank you!</p>',
  };
  sgMail.send(msg);
  console.log("Email sent!");
}

app.get('/manage/*', function(req, res) {
	
	var pathEnd = req.query.page;

//If path ends with 'users' we return all users
  if(pathEnd === 'general'){
	currPage = "manage";
	res.send('manage');
  } else if(pathEnd === 'attendance'){
	currPage = "manage/attendance";
	res.send('attendance');
  } else if(pathEnd === 'moduleAssignment'){
	currPage = "manage/moduleAssignment";
	res.send('moduleAssignment');
  }
});

app.post('/attendance', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var update = req.body;
  var sql = "SELECT * FROM eventassignment INNER JOIN sla ON sla.studentNumber = eventassignment.studentNumber WHERE eventassignment.studentNumber = '" + req.body.sla + "' AND eventassignment.eventID = '" + req.body.eventID + "'";
    
	
	var eventType = "Other";
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
		if (err) throw err;
        //console.log(result);
		
		if(result.length > 0) {
			res.send("duplicate, " + result[0].firstName + " " + result[0].lastName);
        } else {
			var sql2 = "SELECT * FROM sla WHERE sla.studentNumber = '" + req.body.sla + "'; SELECT * FROM events WHERE events.eventID = '" + req.body.eventID + "'; SELECT * FROM reference WHERE reference.studentNumber = '" + req.body.sla + "'";
    
			//Execute the query
			con.query(sql2, function (err, result2) {
				//Check for errors
				if (err) throw err;
				//console.log(result);
				if(result2[0].length > 0) {
					slaToEvent(req.body.eventID, req.body.sla);
					console.log(result2[0][0].firstName + " " + result2[0][0].lastName);
					
					if(result2[1][0].type == "Training") {
					var sql3 = "UPDATE sla SET trained = 'Yes', status = 'Active' WHERE sla.studentNumber = '" + req.body.sla + "'";
					
					if(result2[2][0].startDate == null || result2[2][0].startDate == "") {
						sql3 += "; UPDATE reference SET startDate = '" + result2[1][0].date + "' WHERE reference.studentNumber = '" + req.body.sla + "'";
					}
    
			//Execute the query
			con.query(sql3, function (err, result3) {
				//Check for errors
				if (err) throw err;
				//console.log(result);
				
          
			});
			con.end();
					}
					res.send("new, " + result2[0][0].firstName + " " + result2[0][0].lastName);
				} else {
					console.log("student does not exist, " + req.body.sla);
					res.send("new, student does not exist, " + req.body.sla);
				}
			});
		}
  });
});


app.post('/saveMaharaComment', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//get comment
  var update = req.body;
  
   var sql = "INSERT INTO maharacomments (maharaAssignmentID, comment, rating, userID, dateTime) VALUES ('" + update.maharaID + "','" + update.comment + "'," + update.rating + ",'" + req.session.userName + "','" + update.dateTime + "')";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 //searchResults = result;
			 currPage = 'search/module';
          
  });
  con.end();
});




app.post('/moduleAssignment', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var update = req.body;
  var sql = "SELECT * FROM moduleassignment INNER JOIN sla ON sla.studentNumber = moduleassignment.studentNumber WHERE moduleassignment.studentNumber = '" + req.body.sla + "' AND moduleassignment.crn = '" + req.body.moduleID + "'";
    
	
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
		if (err) throw err;
        //console.log(result);
		
		if(result.length > 0) {
			res.send("duplicate, " + result[0].firstName + " " + result[0].lastName);
        } else {
			var sql2 = "SELECT * FROM sla WHERE sla.studentNumber = '" + req.body.sla + "'; SELECT * FROM modules WHERE modules.crn = '" + req.body.moduleID + "'";
    
			//Execute the query
			con.query(sql2, function (err, result2) {
				//Check for errors
				if (err) throw err;
				//console.log(result);
				if(result2[0].length > 0) {
					slaToModule(req.body.moduleID, req.body.sla);
					console.log(result2[0][0].firstName + " " + result2[0][0].lastName);
					
					res.send("new, " + result2[0][0].firstName + " " + result2[0][0].lastName);
				} else {
					console.log("student does not exist, " + req.body.sla);
					res.send("new, student does not exist, " + req.body.sla);
				}
			});
			con.end();
		}
  });
});

function slaToModule(mod, sla) {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
	
	var sql2 = "INSERT INTO moduleassignment (crn, studentNumber) VALUES ('" + mod + "', '" + sla + "')";
    
			//Execute the query
			con.query(sql2, function (err, result) {
				//Check for errors
				if (err) throw err;
				//console.log(result);
				
          
			});
			con.end();
}


function slaToEvent(ev, sla) {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
	
	var sql2 = "INSERT INTO eventassignment (eventID, studentNumber) VALUES ('" + ev + "', '" + sla + "')";
    
			//Execute the query
			con.query(sql2, function (err, result) {
				//Check for errors
				if (err) throw err;
				//console.log(result);
				
          
			});
			con.end();
}

app.post('/save/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var tab = req.query.tab;
  var studNo = req.query.studentNumber;
  var update = req.body;
  console.log(tab);
  
   var sql = "";
  if(tab == "profile") {
  //Build SQL query
    sql = "UPDATE sla SET studentNumber = '" + update.studentNumber + "', title = '" + update.title + "', firstName = '" + update.firstName + "', lastName = '" + update.lastName + "', dateOfBirth = '" + update.dateOfBirth + "', uniEmail = '" +update.uniEmail + "', personalEmail = '" + update.personalEmail + "', mobileNumber = '" + update.mobileNumber + "' WHERE studentNumber = '" + studNo + "'; UPDATE education SET studentNumber = '" + update.studentNumber + "' WHERE studentNumber = '" + studNo + "'; UPDATE profile SET studentNumber = '" + update.studentNumber + "' WHERE studentNumber = '" + studNo + "'; UPDATE skills SET studentNumber = '" + update.studentNumber + "' WHERE studentNumber = '" + studNo + "'; UPDATE reference SET studentNumber = '" + update.studentNumber + "' WHERE studentNumber = '" + studNo + "'; UPDATE eventassignment SET studentNumber ='" + update.studentNumber + "' WHERE studentNumber = '" + studNo + "'; UPDATE moduleassignment SET studentNumber ='" + update.studentNumber + "' WHERE studentNumber = '" + studNo + "'";
  } else if(tab == "education") {
	//Build SQL query
	sql = "UPDATE education SET programme = '" + update.programme + "', year = '" + update.year + "', school = '" + update.school + "' WHERE studentNumber = '" + studNo + "'";
  } else if(tab == "application") {
	//Build SQL query
	sql = "UPDATE profile SET whySLA = '" + update.whySLA + "', successFormula = '" + update.successFormula + "' WHERE studentNumber = '" + studNo + "'; UPDATE education SET previousModule1 = '" + update.previousModule1 + "', previousModule2 = '" + update.previousModule2 + "', previousModule3 = '" + update.previousModule3 + "', previousModule4 = '" + update.previousModule4 + "' WHERE studentNumber = '" + studNo + "';";
  } else if(tab == "other") {
	//Build SQL query
	sql = "UPDATE profile SET disabilityState = '" + update.disabilityState + "', disabilityNature = '" + update.disabilityNature + "' WHERE studentNumber = '" + studNo + "'";
  } else if(tab == "events") {
	  sql = "SELECT * FROM events";
  } else if(tab == "skills") {
	  sql = "SELECT * FROM skills";
  }
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 //searchResults = result;
			 currPage = 'search/sla';
          
  });
  con.end();
});

app.post('/saveModule/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
  var tab = req.query.tab;
  var studNo = req.query.crn;
  var update = req.body;
  console.log(tab);
  
   var sql = "";
  if(tab == "moduleGen") {
  //Build SQL query
    sql = "UPDATE modules SET crn = '" + update.crn + "', moduleCode = '" + update.moduleCode + "', moduleShortTitle = '" + update.moduleShortTitle + "', moduleLongTitle = '" + update.moduleLongTitle + "', moduleLeaderName = '" + update.moduleLeaderName + "', moduleLeaderEmail = '" +update.moduleLeaderEmail + "' WHERE crn = '" + studNo + "'; UPDATE moduleassignment SET crn = '" + update.crn + "' WHERE crn = '" + studNo + "'";
  } else if(tab == "moduleSLAs") {
	  sql = "SELECT * FROM moduleassignment";
  }
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             //console.log(result);
             res.send(JSON.stringify(result));
			 //searchResults = result;
			 currPage = 'search/module';
          
  });
  con.end();
});


app.get('/searchPage/*', function(req, res) {
	
	var pathEnd = req.query.page;

//If path ends with 'users' we return all users
  if(pathEnd === 'sla'){
	currPage = "search/sla";
	res.send('sla');
  } else if(pathEnd === 'event'){
	currPage = "search/event";
	res.send('event');
  } else if(pathEnd === 'module'){
	currPage = "search/module";
	res.send('module');
  } else if(pathEnd === 'general'){
	currPage = "search";
	res.send('search');
  }
	
	
});





app.get('/dashboard', function(req, res) {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads",
	multipleStatements: true
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
  var sql = "SELECT * FROM staff WHERE networkID='" + req.session.user + "'; SELECT * FROM student WHERE uniEmail='" + req.session.user + "'; SELECT * FROM lecturer WHERE uniEmail='" + req.session.user + "'";
    
//Execute the query
  con.query(sql, function (err, result) {

    //Check for errors
      if (err) throw err;
        
		var user = [];
		if(result[0].length > 0) {
			user = result[0];
		} else if(result[1].length > 0) {
			user = result[1];
			user[0].networkID = result[1][0].uniEmail;
		} else if(result[2].length > 0) {
			user = result[2];
			user[0].networkID = result[2][0].uniEmail;
		}
		
		console.log(JSON.stringify(user));
		if(user[0].role == "Admin" || user[0].role == "PAL" || user[0].role == "SSLA" || user[0].role == "SSLA+") {
			currPage = "dashboard/staff";
			res.send("dashboard/staff");
		} else if(user[0].role == "Student") {
			currPage = "dashboard/student";
			res.send("dashboard/student");
		} else if(user[0].role == "SLA") {
			currPage = "dashboard/sla";
			res.send("dashboard/sla");
		} else if(user[0].role == "Lecturer") {
			currPage = "dashboard/lecturer";
			res.send("dashboard/lecturer");
		}
		
  });
    con.end();
});

app.get('/logout/*', function (req, res) {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  var sql = "";
  
  //Build SQL query
  if(req.query.role == "staff") {
	sql = "UPDATE staff SET lastLogin='" + currentLogin + "' WHERE networkID='" + req.session.user + "'";
  } else {
	sql = "UPDATE " + req.query.role.toLowerCase() + " SET lastLogin='" + currentLogin + "' WHERE uniEmail='" + req.session.user + "'";  
  }
    
//Execute the query
  con.query(sql, function (err, result) {

    //Check for errors
      if (err) throw err;
	  
	  console.log("updated lastLogin.");
  });
	con.end();	
	
  req.session.destroy();
  console.log("session destroyed!");
  res.send("logout success!"); 
  currPage = "dashboard";
  loginAttempts = 0;
  return;
});

app.get('/delete/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
	var deleteObj = req.query.object;

if(deleteObj == "sla") {
  var deleteRecord = req.query.user;
  
  
  //Build SQL query
    var sql = "DELETE FROM education WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM moduleassignment WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM maharaassignment WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM eventassignment WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM profile WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM skills WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM reference WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM sla WHERE studentNumber='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
             
		console.log(result);
		res.send("user deleted!");
          
  });
} else if(deleteObj == "module") {
	var deleteRecord = req.query.module;
  
  
  //Build SQL query
    var sql = "DELETE FROM modules WHERE crn='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM moduleassignment WHERE crn='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
} else if(deleteObj == "evnt") {
	var deleteRecord = req.query.evnt;
  
  
  //Build SQL query
    var sql = "DELETE FROM events WHERE eventID='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
  
  //Build SQL query
    sql = "DELETE FROM eventassignment WHERE eventID='" + deleteRecord + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
          
  });
}
  con.end();
});

app.get('/deleteSlaEvent/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
	var deleteRecord = req.query.studentNumber;
	var eventID = req.query.eventID;
	
  //Build SQL query
    sql = "DELETE FROM eventassignment WHERE studentNumber='" + deleteRecord + "' AND eventID = '" + eventID + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
			
		res.send("OK");
  });
  con.end();
});

app.get('/deleteSlaModule/*', function (req, res) {
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
    
//assign staff details
	var deleteRecord = req.query.studentNumber;
	var moduleID = req.query.moduleID;
	
  //Build SQL query
    sql = "DELETE FROM moduleassignment WHERE studentNumber='" + deleteRecord + "' AND crn = '" + moduleID + "'";
    
//Execute the query
  con.query(sql, function (err, result) {
    //Check for errors
          if (err) throw err;
			
		res.send("OK");
  });
  con.end();
});


app.post('/register', function (req, res){
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors

      //Output results
        console.log("Connected!");
      }
  );

  storestaff(req.body);
  console.log("done!");
    //Close the connection
      console.log("Disconnected!");
  con.end();
  res.send("{OK!}");
  return;
});

app.post('/upload/*', function (req, res){
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors

      //Output results
        console.log("Connected!");
      }
  );
  
  var pathArray = req.url.split("/");
  
  var uploadResults = {};
  if(pathArray[pathArray.length-1] == "sla") {
	  uploadResults = storeSlas(req.body);
	  
	  setTimeout(function(){
  getCountFromDB();
  setTimeout(function() {uploadResults.afterCount = slaDetails.total;
  res.send(JSON.stringify(uploadResults));}, 500);}, 500);
	  
  } else if(pathArray[pathArray.length-1] == "module") {
	  storeModules(req.body);
	  res.send("Uploaded " + req.body.length);
  }
	
	//getCountFromDB();
  //req.body will have an array of SLAs
  //console.log(req.body);
  console.log("done!");
    //Close the connection
      console.log("Disconnected!");
  con.end();
  
  return;
});


  //Function that adds data to database
function storeSlas(slaArray){
	console.log("UPLOAD LENGTH: " + slaArray.length + "\n\n");
	var returnObj = {fileCount: slaArray.length, beforeCount: 0, afterCount: 0, duplicates: 0};
	getCountFromDB();
  returnObj.beforeCount = slaDetails.total;
  console.log("before count in obj" + getCountFromDB());
  //for(i=0; i<slaArray.length-1; i++) {
  //Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });

//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
        if (err) throw err;//Check for errors

        //Output results
          console.log("Connected!");
    }
);
  
for(i=0; i<slaArray.length; i++) {
//Build SQL query
  var sql = "INSERT IGNORE INTO sla (title, firstName, lastName, studentNumber, uniEmail, personalEmail, dateOfBirth, mobileNumber, returningSLA) " + 
  "       VALUES ('" + slaArray[i].title + "', '" + slaArray[i].firstName + "', '" + slaArray[i].lastName + "', '" + slaArray[i].studentNumber + "', '" + slaArray[i].uniEmail + "', '" + slaArray[i].personalEmail + "', '" + slaArray[i].dateOfBirth + "', '" + slaArray[i].mobileNumber + "', '" + slaArray[i].returningSLA + "')";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
    //console.log(result.affectedRows + ' rows updated.');
  });

//Build SQL query
  var sql = "INSERT IGNORE INTO education (studentNumber, programme, year, school) " + 
  "       VALUES ('" + slaArray[i].studentNumber + "', '" + slaArray[i].programme + "', '" + slaArray[i].year + "', '" + slaArray[i].school + "')";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
    //console.log(result.affectedRows + ' rows updated.');
  });  
  
//Build SQL query
  var sql = "INSERT IGNORE INTO profile (studentNumber, disabilityState, disabilityNature, whySLA, successFormula) " + 
  "       VALUES ('" + slaArray[i].studentNumber + "', '" + slaArray[i].disabilityState + "', '" + slaArray[i].disabilityNature + "', '" + slaArray[i].whySLA + "', '" + slaArray[i].successFormula + "')";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
    //console.log(result.affectedRows + ' rows updated.');
  });

//Build SQL query
  var sql = "INSERT IGNORE INTO skills (studentNumber, ITSkills, otherSkills) " + 
  "       VALUES ('" + slaArray[i].studentNumber + "', '" + slaArray[i].itSkills + "', '" + slaArray[i].otherSkills + "')";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
    console.log(result.affectedRows + ' rows updated.');
  });  
  
  //Build SQL query
  var sql = "INSERT IGNORE INTO reference (studentNumber) " + 
  "       VALUES ('" + slaArray[i].studentNumber + "')";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
    console.log(result.affectedRows + ' rows updated.');
  });  
  
  }
  con.end();
  
  returnObj.duplicates = returnObj.fileCount - (returnObj.afterCount - returnObj.beforeCount);
  
  return returnObj;
}

  //Function that adds data to database
function storeModules(moduleArray){
	console.log("UPLOAD LENGTH: " + moduleArray.length + "\n\n");
	
	var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });

//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
        if (err) throw err;//Check for errors

        //Output results
          console.log("Connected!");
    }
);
  
for(i=0; i<moduleArray.length; i++) {
//Build SQL query
  var sql = "REPLACE INTO modules (crn, moduleCode, schoolCode, moduleShortTitle, moduleLongTitle, moduleLeaderName, moduleLeaderEmail) " + 
  "       VALUES ('" + moduleArray[i].crn + "', '" + moduleArray[i].moduleCode + "', '" + moduleArray[i].schoolCode + "', '" + moduleArray[i].moduleShortTitle + "', '" + moduleArray[i].moduleLongTitle + "', '" + moduleArray[i].moduleLeaderName + "', '" + moduleArray[i].moduleLeaderEmail + "')";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
    console.log(result);
  });

}
}

function getSLASchoolFromDB() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM education WHERE school = 'Science & Technology'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaSchool.ST = result[0].total;
  //console.log("COUNT = " + result[0].total);
  });
  
  
   //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM education WHERE school = 'Media & Performing Arts'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaSchool.MP = result[0].total;
  //console.log("COUNT = " + result[0].total);
  });
  
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM education WHERE school = 'Law'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaSchool.LW = result[0].total;
  //console.log("COUNT = " + result[0].total);
  });
  
  
   //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM education WHERE school = 'Health & Education'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaSchool.HE = result[0].total;
  //console.log("COUNT = " + result[0].total);
  });
  
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM education WHERE school = 'Business'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaSchool.BS = result[0].total;
  //console.log("COUNT = " + result[0].total);
  });
  
  
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM education WHERE school = 'Art & Design'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaSchool.AD = result[0].total;
  //console.log("COUNT = " + result[0].total);
  });
	con.end();
		console.log("SCHOOL COUNT: " + JSON.stringify(slaSchool));
}


function getCountFromDB() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM sla";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaDetails.total = result[0].total;
  console.log("TOTAL = " + result[0].total);
  });
	con.end();	
}

function getReturningSLACount() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM sla WHERE returningSLA = 'Yes'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaDetails.returning = result[0].total;
  console.log("RETURNING COUNT = " + result[0].total);
  });
	con.end();	
}

function getTrainedSLACount() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM sla WHERE trained = 'Yes'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaDetails.trained = result[0].total;
  console.log("TRAINED COUNT = " + result[0].total);
  });
	con.end();	
}

function getSeniorCount() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM sla WHERE seniorSLA = 'Yes'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaDetails.senior = result[0].total;
  console.log("SENIOR COUNT = " + result[0].total);
  });
	con.end();	
}

function getWithdrawnCount() {
	//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });
//Connect to the database
  con.connect(
    //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors
      //Output results
        console.log("Connected!");
      }
  );
  
  //Build SQL query
  var sql = "SELECT COUNT(*) as total FROM sla WHERE withdrawn = 'Yes'";

//Execute the query
  con.query(sql, function (err, result) {

  //Check for errors
    if (err) throw err;

  //Output results
  slaDetails.withdrawn = result[0].total;
  console.log("WITHDRAWN COUNT = " + result[0].total);
  });
	con.end();	
}



/* Define the function that will handle GET requests to our web service
This method is only called if the user requests /users     */
function handleGetRequest(request, response){
//Create a connection object with the user details
  var con = mysql.createConnection({
    host: "localhost",
    user: "krys17",
    password: "Kristen",
    database: "slads"
  });

//Connect to the database
con.connect(
  //This function is called when the connection is attempted
    function(err) {
      if (err) throw err;//Check for errors

      //Output results
        console.log("Connected!");
    }
);

//Split the path of the request into its components
  var pathArray = request.url.split("/");

//Get the last part of the path
  var pathEnd = pathArray[pathArray.length - 1];

//If path ends with 'users' we return all users
  if(pathEnd === 'users'){
	//currPage = 'search/sla';
  //Build SQL query
    var sql = "SELECT * FROM sla INNER JOIN education ON education.studentNumber=sla.studentNumber";
    
  //Execute the query
    con.query(sql, function (err, result) {

      //Check for errors
        if (err) throw err;

        response.send(JSON.stringify(result));
    });
  }
  con.end();
}