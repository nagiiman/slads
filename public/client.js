
    ////////////////////////
   //  global variables  //
  ////////////////////////

var slaArray = [];
var moduleArray = [];
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]; 
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    ////////////////////////
   //        setup       //
  ////////////////////////


//Set up page when window has loaded
window.onload = init;

/* Sets up the page */
function init(){ 
  //createUserNavBar();
  checkSession();
  //createAddSLAPage();
}

    ////////////////////////
   //    create pages    //
  ////////////////////////

//footer on every page
function createFooter() {
  //Get a reference to the document body
    var docBody = document.getElementsByTagName("body")[0];//Only one body
    
  footerDiv = document.createElement("div");
  footerDiv.id = "footer";
  para = document.createElement("p");
  para.innerHTML = "© SLADS 2019";
  footerDiv.appendChild(para);
  docBody.appendChild(footerDiv);
}


//create logo bar
function createLogoBar() {
	var logoBar = document.getElementById("logoBar");
	
	logoBar.style = "height: 120px; border-bottom: 10px solid #000;";
	
	var d = new Date(); 
	
	logoBar.innerHTML = '<div class="row" style="margin-left: 0px; margin-right: 0px;"><div class="col-md-2" style="text-align: left;"><img src="images/weblogo.png" id="logo"></div><div class="col-md-8"><br><br><p style="font-size: 20px; text-align: center; width: 100%;">' +	days[d.getDay()] + ", " + d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear() + '</p></div><div class="col-md-2" style="text-align:right;"><img src="images/mdxlogo.jpg" id="mdxlogo" style="padding-top: 10px;"></div></div>';
}


//login page
function createLoginBox() {
	var userLogin = document.createElement("div");
  userLogin.id = "userLogin";
  userLogin.innerHTML = '<center><div id="loginBox" style="padding-top: 60px; position: relative; width:90%"><img src="images/weblogo.png" alt="logo" style="width: 40%;"><div style="background-color: #ffffff99; width:35%; height:60%; position: absolute; left: 32.5%; top: 30%; text-align: center; font-size: 14px; box-shadow: 2px 2px 2px 2px #aaaaaaaa;"><p style="font-size: 20px; padding-top: 10px;"><b>LOGIN</b></p><form><div class="form-row col-md-auto"><label for="inputUsername4">Username</label><input type="text" class="form-control" id="logInUserName" placeholder="User" style="height:30px;"></div><br><div class="form-row col-md-auto"><label for="inputPassword4">Password</label><input type="password" class="form-control" id="logInPassWord" placeholder="Password" style="height:30px;"></div><div class="form-row col-md-auto" style="padding-top: 5px;"><div class="col-md-6"></div><div class="col-md-6"><button onclick="loginStaff()" class="submit">Enter</button></div></div></form><br><p id="loginResult" style="color:red;"></p></div></div></center>';
  
  var contents = document.getElementById("contents");
  contents.innerHTML = "";
  contents.appendChild(userLogin);
}
  
  
//create user side bar
function createUserNavBar(staff) {
	createLogoBar();
	
	var htmlStr = '<div class="row" style="margin-left: 0px; margin-right: 0px;">' + 
				'<div id="userSpace" class="col-md-3" style="text-align: center; background-color: white; border-right: dashed 2px silver">' + 
					'<div class="row" style="padding: 10px 10px 10px 10px; margin-left: 0px; margin-right: 0px; width: 100%">' + 
						'<img id="profilePic" src="images/adminProfile.png" style="display: block; margin-left: auto; margin-right: auto; width: 50%; height: 50%; margin-bottom: 10px;">' + 
						'<p style="font-size: 22px; width: 100%; margin-bottom: 5px;">Welcome <span id="userFirstName">' + staff.firstName + '</span>!</p>' + 
						'<p style="font-size: 16px; width: 100%; margin-bottom: 5px;">Logged in as: <span id="userRole">' +staff.role +  '</span></p>' + 
						'<p style="font-size: 16px; width: 100%;">Last Login: <span id="userLastLogin">';
						
						if(staff.lastLogin != null) {
							htmlStr += staff.lastLogin;
						} else {
							htmlStr += " - ";
						}

						htmlStr += '</span></p>' + 
					'</div>' + 
					'<div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestDashboard()" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_dashboard.png" style="width: 20%; height: 20%; padding: 5px 0px 5px 0px;"> Dashboard</button></p>' + 
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestAdd(\'general\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_add.png" style="width: 25%; height: 25%; padding: 5px 0px 5px 0px;"> Add</button></p>' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestSearch(\'general\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_search.png" style="width: 22%; height: 22%; padding: 5px 0px 5px 0px;"> Search</button></p>' + 
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestManage(\'general\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_manage.png" style="width: 20%; height: 20%; padding: 5px 0px 5px 0px;"> Manage</button></p>' + 
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_edit.png" style="width: 18%; height: 18%; padding: 5px 0px 5px 0px;"> Edit Details</button></p>' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="logout(\'staff\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_logout.png" style="width: 22%; height: 22%; padding: 5px 0px 5px 0px;"> Logout</button></p>' +
					'</div></div>' + 
				'<div class="col-md-9" id="changePage" style="height:550px;"">' + 
				'</div></div>';
  
  var contents = document.getElementById("contents");
  
  contents.innerHTML = htmlStr; 
}


function createLecturerNavBar(lecturer) {
	createLogoBar();
	
	var htmlStr = '<div class="row" style="margin-left: 0px; margin-right: 0px;">' + 
				'<div id="userSpace" class="col-md-3" style="text-align: center; background-color: white; border-right: dashed 2px silver">' + 
					'<div class="row" style="padding: 10px 10px 10px 10px; margin-left: 0px; margin-right: 0px; width: 100%">' + 
						'<img id="profilePic" src="images/adminProfile.png" style="display: block; margin-left: auto; margin-right: auto; width: 50%; height: 50%; margin-bottom: 10px;">' + 
						'<p style="font-size: 22px; width: 100%; margin-bottom: 5px;">Welcome <span id="userFirstName">' + lecturer.firstName + '</span>!</p>' + 
						'<p style="font-size: 16px; width: 100%; margin-bottom: 5px;">Logged in as: <span id="userRole">' +lecturer.role +  '</span></p>' + 
						'<p style="font-size: 16px; width: 100%;">Last Login: <span id="userLastLogin">';
						
						if(lecturer.lastLogin != null) {
							htmlStr += lecturer.lastLogin;
						} else {
							htmlStr += " - ";
						}

						htmlStr += '</span></p>' + 
					'</div>' + 
					'<div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestDashboard()" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_dashboard.png" style="width: 20%; height: 20%; padding: 5px 0px 5px 0px;"> Dashboard</button></p>' + 
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestNomination(\'one\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_nomination.png" style="width: 37%; height: 37%; padding: 5px 0px 5px 13px;">Nomination</button></p>' +
						'<button onclick="logout(\'lecturer\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_logout.png" style="width: 22%; height: 22%; padding: 5px 0px 5px 0px;"> Logout</button></p>' +
					'</div></div>' + 
				'<div class="col-md-9" id="changePage" style="height:550px;"">' + 
				'</div></div>';
  
  var contents = document.getElementById("contents");
  
  contents.innerHTML = htmlStr; 
}

function createStudentSLANavBar(student) {
	createLogoBar();
	
	var htmlStr = '<div class="row" style="margin-left: 0px; margin-right: 0px;">' + 
				'<div id="userSpace" class="col-md-3" style="text-align: center; background-color: white; border-right: dashed 2px silver">' + 
					'<div class="row" style="padding: 10px 10px 10px 10px; margin-left: 0px; margin-right: 0px; width: 100%">' + 
						'<img id="profilePic" src="images/adminProfile.png" style="display: block; margin-left: auto; margin-right: auto; width: 50%; height: 50%; margin-bottom: 10px;">' + 
						'<p style="font-size: 22px; width: 100%; margin-bottom: 5px;">Welcome <span id="userFirstName">' + student.firstName + '</span>!</p>' + 
						'<p style="font-size: 16px; width: 100%; margin-bottom: 5px;">Logged in as: <span id="userRole">' +student.role +  '</span></p>' + 
						'<p style="font-size: 16px; width: 100%;">Last Login: <span id="userLastLogin">';
						
						if(student.lastLogin != null) {
							htmlStr += student.lastLogin;
						} else {
							htmlStr += " - ";
						}

						htmlStr += '</span></p>' + 
					'</div>' + 
					'<div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestDashboard(\'student\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_dashboard.png" style="width: 20%; height: 20%; padding: 5px 0px 5px 0px;"> Dashboard</button></p>' + 
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_application.png" style="width: 35%; height: 35%; padding: 5px 0px 5px 15px;"> Application</button></p>' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="logout(\'student\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_logout.png" style="width: 22%; height: 22%; padding: 5px 0px 5px 0px;"> Logout</button></p>' +
					'</div></div>' + 
				'<div class="col-md-9" id="changePage" style="height:550px;"">' + 
				'</div></div>';
  
  var contents = document.getElementById("contents");
  
  contents.innerHTML = htmlStr; 
}


function createStudentSLANavBar(student) {
	createLogoBar();
	
	var htmlStr = '<div class="row" style="margin-left: 0px; margin-right: 0px;">' + 
				'<div id="userSpace" class="col-md-3" style="text-align: center; background-color: white; border-right: dashed 2px silver">' + 
					'<div class="row" style="padding: 10px 10px 10px 10px; margin-left: 0px; margin-right: 0px; width: 100%">' + 
						'<img id="profilePic" src="images/adminProfile.png" style="display: block; margin-left: auto; margin-right: auto; width: 50%; height: 50%; margin-bottom: 10px;">' + 
						'<p style="font-size: 22px; width: 100%; margin-bottom: 5px;">Welcome <span id="userFirstName">' + student.firstName + '</span>!</p>' + 
						'<p style="font-size: 16px; width: 100%; margin-bottom: 5px;">Logged in as: <span id="userRole">' +student.role +  '</span></p>' + 
						'<p style="font-size: 16px; width: 100%;">Last Login: <span id="userLastLogin">';
						
						if(student.lastLogin != null) {
							htmlStr += student.lastLogin;
						} else {
							htmlStr += " - ";
						}

						htmlStr += '</span></p>' + 
					'</div>' + 
					'<div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="requestDashboard(\'student\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_dashboard.png" style="width: 20%; height: 20%; padding: 5px 0px 5px 0px;"> Dashboard</button></p>' + 
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_application.png" style="width: 35%; height: 35%; padding: 5px 0px 5px 15px;"> Application</button></p>' +
						'<p style="width: 100%; text-align: left; margin-bottom: 0px;"><button onclick="logout(\'student\')" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_logout.png" style="width: 22%; height: 22%; padding: 5px 0px 5px 0px;"> Logout</button></p>' +
					'</div></div>' + 
				'<div class="col-md-9" id="changePage" style="height:550px;"">' + 
				'</div></div>';
  
  var contents = document.getElementById("contents");
  
  contents.innerHTML = htmlStr; 
}




function createStaffDashboard(staff, eventArr, slaDet, slaSkl) {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar(staff);
	}
	
	var htmlStr = '<div class="row" style="padding: 10px 10px 10px 10px;">' + 
						'<div class="col-md-6" style="border-style: double; border-radius:5px;">' +
							'<p style="font-size:18px;">Today: </p>' + 
							'<div class="row">' +
								'<div class="col-md-2">' + 
									'<img src="images/icon_noEvent.png" id="todayEventIcon" style="width: 50px;">' +
								'</div>' +
								'<div class="col-md-10">' + 
									'<p id="events" style="font-size: 16px; display: inline-block; margin-bottom: 5px;">' +
										'<span id="todayEventName">No events today.</span>' +
										'<br>' + 
										'<span id="todayEventDate">&nbsp </span>' +
										'<br>' +
										'<span id="todayEventTime">&nbsp </span>' +
									'</p>' +
									'<p style="width: 100%; text-align: right; margin-bottom: 5px;"><button onclick="" id="todayEventButton" style="border: none; background-color: #00000000; font-size: 12px;"> View All Events</button></p>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="col-md-1" style="">' +
						'</div>' + 
						'<div class="col-md-5" style="border-style: double; border-radius:5px;">' +
							'<p style="font-size:18px;">Next Event: </p>' +
							'<div class="row">' +
								'<div class="col-md-2">' +
									'<img src="images/icon_noEvent.png" id="nextEventIcon" style="width: 50px;">' +
								'</div>' +
								'<div class="col-md-10">' +
									'<p id="events" style="font-size: 16px; display: inline-block; margin-bottom: 5px;">' +
										'<span id="nextEventName">No Upcoming Events.</span>' +
										'<br>' +
										'<span id="nextEventDate">&nbsp </span>' +
										'<br>' +
										'<span id="nextEventTime">&nbsp </span>' +
									'</p>' +
									'<p style="width: 100%; text-align: right; margin-bottom: 5px;"><button onclick="" id="nextEventButton" style="border: none; background-color: #00000000; font-size: 12px;"> View Event</button></p>' +
								'</div></div></div></div>' +
					'<div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<div class="col-md-7" style="border-style: double; border-radius:5px;">' +
							'<div class="row">' +
								'<div class="col-md-1">' +
									'<img src="images/icon_dashboard.png" style="width: 40px; display: inline-block; padding-top: 10px;">' +
								'</div>' +
								'<div class="col-md-11">' +
									'<p style="font-size: 20px; margin-top: 18px;">Dashboard</p>' +
								'</div></div>' +
							'<div class="row" style="padding: 15px 15px 15px 15px; font-size: 30px; font-weight: bold; display: block;">' +
								'<p style="margin-bottom: 20px;">Number of trained SLAs: <span id="trainedSLAsNumber">' + slaDet.trained + '</span></p>' +
								'<p style="margin-bottom: 20px;">Number of returning SLAs: <span id="returningSLAsNumber">' + slaDet.returning + '</span></p>'  +
								'<p style="margin-bottom: 20px;">Number of senior SLAs: <span id="seniorSLAsNumber">' + slaDet.senior + '</span></p>' +
								'<p style="margin-bottom: 20px;">Number of withdrawn SLAs: <span id="withdrawnSLAsNumber">' + slaDet.withdrawn + '</span></p>' +
								'<p style="margin-bottom: 20px;">Total SLAs: <span id="totalSLAsNumber">' + slaDet.total + '</span></p>' +
							'</div></div>' + '<div class="col-md-1">&nbsp</div><div class="col-md-4" style="border-style: double; border-radius:5px; text-align:center; font-size: 30px;">' + '<div class="row" style="height:33%; font-weight: bold;">' + '<div class="col-md-6">' + '<p style="margin: 10px 10px 10px 10px;">AD</p><p>'+ slaSkl.AD +'</p>' + '</div>' + '<div class="col-md-6">' + '<p style="margin: 10px 10px 10px 10px;">BS</p><p>'+ slaSkl.BS +'</p>' + '</div>' + '</div>' + '<div class="row" style="height:33%; font-weight: bold;">' + '<div class="col-md-6">' + '<p style="margin: 10px 10px 10px 10px;">HE</p><p>'+ slaSkl.HE +'</p>' + '</div>' + '<div class="col-md-6">' + '<p style="margin: 10px 10px 10px 10px;">LW</p><p>'+ slaSkl.LW +'</p>' + '</div>' + '</div>' + '<div class="row" style="height:33%; font-weight: bold;">' + '<div class="col-md-6">' + '<p style="margin: 10px 10px 10px 10px;">MP</p><p>'+ slaSkl.MP +'</p>' + '</div>' + '<div class="col-md-6">' + '<p style="margin: 10px 10px 10px 10px;">ST</p><p>'+ slaSkl.ST +'</p>' + '</div>' + '</div>' + '</div>' + 
							'</div></div></div>';
							
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
  
  updateEvents(eventArr);
}


function createLecturerDashboard(lecturer) {
	if(document.getElementById("userSpace") == undefined) {
		createLecturerNavBar(lecturer);
	}
	
	var htmlStr = '<div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<div class="col-md-12" style="border-style: double; border-radius:5px;">' +
							'<div class="row">' +
								'<div class="col-md-1">' +
									'<img src="images/icon_dashboard.png" style="width: 40px; display: inline-block; padding-top: 10px;">' +
								'</div>' +
								'<div class="col-md-11">' +
									'<p style="font-size: 20px; margin-top: 18px;">Dashboard</p>' +
								'</div></div>' +
							'<div class="row" style="padding: 15px 15px 15px 15px; font-size: 18px; display: block;"><p>SLAs facilitate student learning by providing an engaging and collaborative environment where students are free to discuss, question and enquire and practise course content. They help students become more engaged with their programmes and provide tips on how to be successful students. They support students in class sessions as well as out of class. The SLA programme is managed by the Peer Assisted Learning (PAL) Team. For more information, contact Simbo Ajayi, Peer Assisted Learning Manager - S.Ajayi@mdx.ac.uk</p>' +
							'</div></div>' + '</div></div></div>';
							
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

function createStudentDashboard(student) {
	if(document.getElementById("userSpace") == undefined) {
		createStudentSLANavBar(student);
	}
	
	var htmlStr = '<div class="row" style="padding: 10px 10px 10px 10px;">' + '<div class="col-md-6" style="border-style: double; border-radius:5px;">' +
							'<p style="font-size:18px;">Applicant Status: </p>' + 'STUDENT.STATUS (NL)' +
							'</p>' +
						'</div>' +
						'<div class="col-md-1" style="">' +
						'</div>' + 
						'<div class="col-md-5" style="border-style: double; border-radius:5px;">' +
							'<p style="font-size:18px;">Application Status: </p>' + 'APPLICATION.STATUS (NL)' +
							'</p>' +
								'</div></div><div class="row" style="padding: 10px 10px 10px 10px;">' +
						'<div class="col-md-12" style="border-style: double; border-radius:5px;">' +
							'<div class="row">' +
								'<div class="col-md-1">' +
									'<img src="images/icon_dashboard.png" style="width: 40px; display: inline-block; padding-top: 10px;">' +
								'</div>' +
								'<div class="col-md-11">' +
									'<p style="font-size: 20px; margin-top: 18px;">Dashboard</p>' +
								'</div></div>' +
							'<div class="row" style="padding: 15px 15px 15px 25px; font-size: 18px; display: block; margin-bottom: -10px;"><p><strong>The SLA scheme is a student to student approach to:</strong></p><ul><li>Improving academic skills and practice.</li><li>Increasing new students\' engagement with their programme.</li><li>Enhancing students\' experience of University life.</li><li>Helping students develop the skills they need to become effective independent learners.</li><li>Improving grades and reducing student drop-out.</li></ul><p><strong>Benefits to students:</strong></p><ul><li>Improves students\' study skills and enables them to become independent learners.</li><li>Students adjust quickly to the demands of university and improve their confidence.</li><li>SLAs motivate students and guide them through the learning process.</li><li>Students feel comfortable asking questions about their programmes and modules.</li><li>Provides opportunity for feedback to academics.</li></ul>' +
							'</div></div>' + '</div></div></div></div>';
							
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


//search sla page
function createSearchPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<center style="padding-top: 40px;"><h3>Search</h3><button onclick="requestSearch(\'sla\')" class="btn-lg btn-danger">SLA</button><br><br><button onclick="requestSearch(\'staff\')" class="btn-lg btn-primary">STAFF</button><br><br><button onclick="requestSearch(\'event\')" class="btn-lg btn-danger">EVENT</button><br><br><button onclick="requestSearch(\'module\')" class="btn-lg btn-danger">MODULE</button><br><br><button onclick="requestAdd(\'student\')" class="btn-lg btn-primary">STUDENT</button><br><br><button onclick="requestSearch(\'lecturer\')" class="btn-lg btn-danger">LECTURER</button></center>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


function createSearchSLAPage() {
	var htmlStr = '<div class="row" id="search" style="border-bottom: double 5px black;"><div class="col-md-12" style=""><div class="row" style="padding-left: 10px; padding-top: 5px;"><div class="col-md-12"><h3>SLA Search</h3></div></div>' + 
	'<div class="row" style="text-align: left; padding: 5px 5px 5px 5px;"><div class="form-inline" style="font-size: 16px; width: 100%;"><div class="col-md-4" style="padding-left: 5px; padding-right: 5px;"><p style="margin-bottom: 10px; margin-top: 10px;">Search By: &nbsp <select id="searchBy"><option value="all">Select...</option><option value="studentNumber">Student Number</option><option value="firstName">First Name</option><option value="lastName">Last Name</option></select></p></div><div class="col-md-5" style="padding: 5px 5px 5px 5px;"><p style="margin-bottom: 10px; margin-top: 10px;">Search Term:  &nbsp <input type="text" id="searchCriteria" placeholder="criteria"style="width:50%;"></p></div><div class="col-md-3" style=""><button onclick="searchSLA(\'list\')" class="btn btn-info submit" style="width: 80px;">Search</button>&nbsp&nbsp&nbsp<button onclick="clearSearch()" class="btn btn-info submit" style="width: 80px;">Clear</button></div></div>' + 
	'</div></div></div><div class="row" id="searchResponse" style="height: 77%;"><div class="col-md-3" id="filter" style="padding: 5px 5px 5px 5px; border-right: solid 1px black;"><div class="row" style="margin: 2px 2px 2px 2px;"><h3>Filter by:</h3></div><div class="row" style="margin: 2px 2px 2px 2px;"><h5>School<h5></div><input type="checkbox" id="artDesign" onchange="searchSLA(\'list\')"><img src="images/module_artDesign.png" alt="Submit"><br><input type="checkbox" id="business" onchange="searchSLA(\'list\')"><img src="images/module_business.png" alt="Submit"><br><input type="checkbox" id="healthEducation" onchange="searchSLA(\'list\')"><img src="images/module_healthEducation.png" alt="Submit"><br><input type="checkbox" id="law" onchange="searchSLA(\'list\')"><img src="images/module_law.png" alt="Submit"><br><input type="checkbox" id="mediaPerformingArts" onchange="searchSLA(\'list\')"><img src="images/module_mediaPerformingArts.png" alt="Submit"><br><input type="checkbox" id="scienceTechnology" onchange="searchSLA(\'list\')"><img src="images/module_scienceTechnology.png" alt="Submit"></div>' + '<div class="col-md-9" id="searchResults" style="height: 100%; overflow: scroll;"><div class="row" style="margin: 2px 2px 2px 2px;"><div class="col-md-10"><h3>Search Results<span id="searchResultCount"></span></h3></div><div class="col-md-2" style="padding-top: 3px;"><div class="row"><button onclick="searchSLA(\'grid\')" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_grid.png"  style="width: 100%; height: 100%;"></button><button onclick="searchSLA(\'list\')" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_list.png" style="width: 100%; height: 100%;"></button><button onclick="downloadSearch()" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_export.png" style="width: 100%; height: 100%;"></button></div></div></div><div class="row" style="margin: 2px 2px 2px 2px;">' + '<div class="col-md-12" id="displayResults"></div></div></div></div></div></div></div>';
	
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
  
  searchSLA("list");
}

function createSearchModulePage() {
	var htmlStr = '<div class="row" id="search" style="border-bottom: double 5px black;"><div class="col-md-12" style=""><div class="row" style="padding-left: 10px; padding-top: 5px;"><div class="col-md-12"><h3>Module Search</h3></div></div>' + 
	'<div class="row" style="text-align: left; padding: 5px 5px 5px 5px;"><div class="form-inline" style="font-size: 16px; width: 100%;"><div class="col-md-4" style="padding-left: 5px; padding-right: 5px;"><p style="margin-bottom: 10px; margin-top: 10px;">Search By: &nbsp <select id="searchBy"><option value="all">Select...</option><option value="crn">CRN</option><option value="moduleCode">Module Code</option><option value="moduleShortTitle">Module Title</option><option value="moduleLeaderName">Module Leader</option></select></p></div><div class="col-md-5" style="padding: 5px 5px 5px 5px;"><p style="margin-bottom: 10px; margin-top: 10px;">Search Term:  &nbsp <input type="text" id="searchCriteria" placeholder="criteria"style="width:50%;"></p></div><div class="col-md-3" style=""><button onclick="searchModule(\'list\')" class="btn btn-info submit" style="width: 80px;">Search</button>&nbsp&nbsp&nbsp<button onclick="clearSearch()" class="btn btn-info submit" style="width: 80px;">Clear</button></div></div>' + 
	'</div></div></div><div class="row" id="searchResponse" style="height: 77%;"><div class="col-md-3" id="filter" style="padding: 5px 5px 5px 5px; border-right: solid 1px black;"><div class="row" style="margin: 2px 2px 2px 2px;"><h3>Filter</h3></div><div class="row" style="margin: 2px 2px 2px 2px;">Filter options</div></div>' + '<div class="col-md-9" id="searchResults" style="height: 100%; overflow: scroll;"><div class="row" style="margin: 2px 2px 2px 2px;"><div class="col-md-10"><h3>Search Results<span id="searchResultCount"></span></h3></div><div class="col-md-2" style="padding-top: 3px;"><div class="row"><button onclick="searchModule(\'grid\')" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_grid.png"  style="width: 100%; height: 100%;"></button><button onclick="searchModule(\'list\')" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_list.png" style="width: 100%; height: 100%;"></button><button onclick="downloadModuleSearch()" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_export.png" style="width: 100%; height: 100%;"></button></div></div></div><div class="row" style="margin: 2px 2px 2px 2px;">' + '<div class="col-md-12" id="displayResults"></div></div></div></div></div></div></div>';
	
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
  
  searchModule("list");
}

function createSearchEventPage() {
	var htmlStr = '<div class="row" id="search" style="border-bottom: double 5px black;"><div class="col-md-12" style=""><div class="row" style="padding-left: 10px; padding-top: 5px;"><div class="col-md-12"><h3>Event Search</h3></div></div>' + 
	'<div class="row" style="text-align: left; padding: 5px 5px 5px 5px;"><div class="form-inline" style="font-size: 16px; width: 100%;"><div class="col-md-4" style="padding-left: 5px; padding-right: 5px;"><p style="margin-bottom: 10px; margin-top: 10px;">Search By: &nbsp <select id="searchBy"><option value="all">Select...</option><option value="name">Name</option><option value="type">Type</option><option value="date">Completed</option></select></p></div><div class="col-md-5" style="padding: 5px 5px 5px 5px;"><p style="margin-bottom: 10px; margin-top: 10px;">Search Term:  &nbsp <input type="text" id="searchCriteria" placeholder="criteria"style="width:50%;"></p></div><div class="col-md-3" style=""><button onclick="searchEvent(\'list\')" class="btn btn-info submit" style="width: 80px;">Search</button>&nbsp&nbsp&nbsp<button onclick="clearSearch()" class="btn btn-info submit" style="width: 80px;">Clear</button></div></div>' + 
	'</div></div></div><div class="row" id="searchResponse" style="height: 77%;"><div class="col-md-3" id="filter" style="padding: 5px 5px 5px 5px; border-right: solid 1px black;"><div class="row" style="margin: 2px 2px 2px 2px;"><h3>Filter</h3></div><div class="row" style="margin: 2px 2px 2px 2px;">Filter options</div></div>' + '<div class="col-md-9" id="searchResults" style="height: 100%; overflow: scroll;"><div class="row" style="margin: 2px 2px 2px 2px;"><div class="col-md-10"><h3>Search Results<span id="searchResultCount"></span></h3></div><div class="col-md-2" style="padding-top: 3px;"><div class="row"><button onclick="searchModule(\'grid\')" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_grid.png"  style="width: 100%; height: 100%;"></button><button onclick="searchModule(\'list\')" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_list.png" style="width: 100%; height: 100%;"></button><button onclick="downloadModuleSearch()" style="border: solid 1px silver; background-color: #00000000; width: 30px; height: 30px;"><img src="images/icon_export.png" style="width: 100%; height: 100%;"></button></div></div></div><div class="row" style="margin: 2px 2px 2px 2px;">' + '<div class="col-md-12" id="displayResults"></div></div></div></div></div></div></div>';
	
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
  
  searchEvent("list");
}


//add sla page
function createAddPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<center style="padding-top: 40px;"><h3>Add</h3><button onclick="requestAdd(\'sla\')" class="btn-lg btn-danger">SLA</button><br><br><button onclick="requestAdd(\'staff\')" class="btn-lg btn-danger">STAFF</button><br><br><button onclick="requestAdd(\'event\')" class="btn-lg btn-danger">EVENT</button><br><br><button onclick="requestAdd(\'module\')" class="btn-lg btn-danger">MODULE</button><br><br><button onclick="requestAdd(\'student\')" class="btn-lg btn-danger">STUDENT</button><br><br><button onclick="requestAdd(\'lecturer\')" class="btn-lg btn-danger">LECTURER</button><br><br></center>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


//add sla page
function createAddSLAPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<button onclick="requestAdd(\'general\')" class="btn btn-default">Back</button><br><br><br><h3>Upload csv/txt file with SLA details.</h3><br><br>'+
  '<input type="file" id="fileUpload" class="inputFileFields"><button onclick="uploadSLAFile()" class="submit">Upload</button><p id="update" style="font-size:12; color:red;"></p>'+
  '<br><script>$(document).ready(function () {var date_input = $(\'input[name="date"]\');var container = $(\'.bootstrap-iso form\').length > 0 ? $(\'.bootstrap-iso form\').parent() : "body";var options = {format: \'mm/dd/yyyy\',container: container,todayHighlight: true,autoclose: true,};date_input.datepicker(options);})var wordLen = 150;function checkWordLen(obj){var len = obj.value.split(/[\s]+/);if(len.length > wordLen){alert("You cannot put more than "+wordLen+" words in this text area.");obj.oldValue = obj.value!=obj.oldValue?obj.value:obj.oldValue;obj.value = obj.oldValue?obj.oldValue:"";return false;}return true;}$(function() {var update = function() {$(\'#serializearray\').text(JSON.stringify($(\'form\').serializeArray()));$(\'#serialize\').text(JSON.stringify($(\'form\').serialize()));};update();$(\'form\').change(update);})</script>'+
  '<div style=" overflow: scroll; height: 410px;"><form id="myForm"><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Title</label><select class="custom-select mr-sm-2" id="salutation"><option selected>Choose...</option><option value="Mr.">Mr.</option><option value="Mrs.">Mrs.</option><option value="Ms.">Ms.</option></select></div><div class="col-auto"><label for="inputName4">First name</label><input type="text" class="form-control" placeholder="John" required id = "firstName"></div><div class="col-auto"><label for="validationServer02">Last name</label><input type="text" class="form-control" id="lastName" placeholder="Smith" required></div></div><div class="form-row col-auto"><div class="col-auto"><label class="control-label" for="date">Date of Birth</label><input class="form-control" id="dob" name="date" placeholder="MM/DD/YYY" type="text" required /></div>'+
  '<div class="col-auto"><label for="inputStudentNumber">Student Number</label><input type="text" class="form-control" placeholder="M00XXXXXX" required id = "studentNumber"></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">University Email</label><input type="email" class="form-control" placeholder="AB123@live.mdx.ac.uk" required id ="uniEmail"></div><div class="col-auto"><label for="inputEmail4">Personal Email</label><input type="email" class="form-control" placeholder="jon.smith@myemail.com" required id ="email"></div>'+
  '<div class="form-group col-auto"><label for="inputPassword4">Mobile Number</label><input type="text" class="form-control" placeholder="07XXXXXXXXX" required id ="phoneNumber"></div></div><div class="form-row col-auto"><div class="col-md-4"><label for="inputEmail4">Would you describe yourself as diabled?</label><select class="custom-select mr-sm-2" id="diabled?">option selected required>Please choose...</option><option value="Yes">Yes</option><option value="No">No</option><option value="I do not wish to disclose">I do not wish to disclose</option></select></div><div class="col-md-6"><label for="inputName4">If yes, please choose the nature of the disability</label><input type="text" class="form-control" required id= "disabilityType"></div></div>'+
  '<div class="form-row col-auto"><div class="col-md-4"><label for="inputName4">Programme of study</label><input type="text" class="form-control" required id= "course"></div><div class="col-md-4"><label for="inputEmail4">Year of study</label><select class="custom-select mr-sm-2" id="year"><option selected required>Please choose...</option><option value="Undergraduate Year 1">Undergraduate Year 1</option><option value="Undergraduate Year 2">Undergraduate Year 2</option><option value="Undergraduate Year 3">Undergraduate Year 3</option><option value="Undergraduate Year 4">Undergraduate Year 4</option><option value="Postgraduate Full time Year 1">Postgraduate Full time Year 1</option><option value="Postgraduate Part time Year 1">Postgraduate Part time Year 1</option><option value="Postgraduate Part time Year 2">Postgraduate Part time Year 2</option></select></div>'+
  '<div class="col-md-4"><label for="inputEmail4">Area/Discipline of study</label><select class="custom-select mr-sm-2" id="department"><option selected required>Please choose...</option><option value="Art & Design">Art & Design</option><option value="Business">Business</option><option value="Health & Education">Health & Education</option><option value="Media & Performing Arts">Media & Performing Arts</option><option value="Science & Technology">Science & Technology</option></select></div></div><div class="form-row col-auto"><div class="col-md-6"><label for="inputEmail4" required>Does your programme include a foundation year?</label><select class="custom-select mr-sm-2" id="foundation"><option selected>Please choose...</option><option value="Yes">Yes</option><option value="No">No</option></select></div></div><div class="form-group col-auto"><label for="inputEmail4">IT Skills (select all that apply)</label><div class="form-row col-auto"><div class="form-check"><input class="form-check-input" type="checkbox" id="ITskill1" value = "Spreadsheets (e.g. Excel)"><label class="form-check-label" for="gridCheck">Spreadsheets (e.g. Excel) &nbsp&nbsp&nbsp&nbsp</label></div>'+
  '<div class="form-check"><input class="form-check-input" type="checkbox" id="ITskill2" value = "Database development (e.g. Access, SQL, Oracle)"><label class="form-check-label" for="gridCheck">Database development (e.g. Access, SQL, Oracle) &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="ITskill3" value = "Web Design (e.g. Access, SQL, Oracle)"><label class="form-check-label" for="gridCheck">Web Design (e.g. Access, SQL, Oracle) &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="ITskill4" value = "Advanced Graphics (e.g. Excel, Minitab, SPSS)"><label class="form-check-label" for="gridCheck">Advanced Graphics (e.g. Excel, Minitab, SPSS) &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="ITskill5" value = "HTML"><label class="form-check-label" for="gridCheck">HTML &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="ITskill6" value = "Web Authoring Skills"><label class="form-check-label" for="gridCheck">Web Authoring Skills &nbsp&nbsp&nbsp&nbsp</label></div></div><div class="col-md-4"><label for="inputName4">Other(please seperate each skill with a comma)</label><input type="text" class="form-control" required id="ITskills"></div></div><div class="form-group col-auto"><label for="inputEmail4">Other skills and work experience (select all that apply)</label>'+
  '<div class="form-row col-auto"><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills1" value ="Academic/Creative Writing"><label class="form-check-label" for="gridCheck">Academic/Creative Writing &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills2" value ="Copywriting/Content Editing"><label class="form-check-label" for="gridCheck">Copywriting/Content Editing &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills3" value="Photography"><label class="form-check-label" for="gridCheck">Photography &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills4" value="Videography"><label class="form-check-label" for="gridCheck">Videography &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills5" value="Advanced Graphics (e.g. Excel, Minitab, SPSS)"><label class="form-check-label" for="gridCheck">Advanced Graphics (e.g. Excel, Minitab, SPSS) &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills6" value=" Visual Content Design (e.g. graphics, illustration)"><label class="form-check-label" for="gridCheck">Visual Content Design (e.g. graphics, illustration) &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills7" value="Creative Software (e.g. Adobe Creative Suite)"><label class="form-check-label" for="gridCheck">Creative Software (e.g. Adobe Creative Suite) &nbsp&nbsp&nbsp&nbsp</label></div><div class="form-check"><input class="form-check-input" type="checkbox" id="otherSkills8" value="Maths"><label class="form-check-label" for="gridCheck">Maths &nbsp&nbsp&nbsp&nbsp</label></div></div><div class="col-md-4"><label for="inputName4">Other</label><input type="text" class="form-control" required id="otherSkills"></div></div>'+
  '<div class="input-group col-auto"><label for="inputName4">Have you worked as an SLA before? </label><div class="input-group-prepend"><label for="inputName4">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  Yes &nbsp&nbsp&nbsp </label><div class="input-group-text"><input type="radio" aria-label="Yes" value="Yes" required name="radiobutton" id= "slaExpYes"></div></div><div class="input-group-prepend"><label for="inputName4">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp No &nbsp&nbsp&nbsp</label><div class="input-group-text"><input type="radio" aria-label="No" value="No" required name="radiobutton" id= "slaExpNo"></div></div></div><div class="form-row col-auto"><label for="inputName4">If yes, please state all the modules you have supported:</label></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Module 1</label><input type="text" class="form-control" placeholder="CSD1234" id="SlaMod1"></div><div class="col-auto"><label for="inputEmail4">Module 2</label><input type="text" class="form-control" placeholder="MSO1234" id="SlaMod2"></div><div class="col-auto"><label for="inputEmail4">Module 3</label><input type="text" class="form-control" placeholder="FSH1234" id="SlaMod3"></div><div class="col-auto"><label for="inputEmail4">Module 4</label> <input type="text" class="form-control" placeholder="BIS1234" id="SlaMod4"></div></div><br><div class="form-group col-auto"><label for="inputAddress">Why do you think you would be a suitable SLA considering skills and responsibilities required for this role?</label><textarea class="form-control" rows="3" onchange="checkWordLen(this);" placeholder="150 words max." required id="slaSkills"></textarea></div>'+'<div class="form-group col-auto"><label for="inputAddress">Based on your own student experience and learning methods, what would you consider to be your ‘success formula?</label><textarea class="form-control" rows="3" onchange="checkWordLen(this);" placeholder="150 words max." required id="slaSuccess"></textarea></div></div><div class="col-auto"><button type="submit" class="btn btn-primary col-auto" >Submit</button></div></form></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

//add sla page
function createAddStaffPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<div id="main" style="width:800px; margin:0 auto;"><form id="myForm"><div class="form-row col-auto"><div class="col-auto"><label for="inputName4">Name of the Event</label><input type="text" class="form-control" required id = "eventName"></div><div class="col-md-4"><label for="inputEmail4">Type of Event</label><select class="custom-select mr-sm-2" id="role"><option selected required>Please choose...</option><option value="Training">Training</option><option value="Observation">Observation</option><option value="Other">Other</option></select></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Tutor</label><input type="text" class="form-control" required id ="tutor"></div><div class="col-auto"><label class="control-label" for="date">Date of the Event</label><input class="form-control" id="dob" name="date" placeholder="MM/DD/YYY" type="text" required /></div></div><div class="form-row col-auto"><div class="col-auto"><label for="example-time-input">Start Time</label><input class="form-control" type="time" value="13:45:00" id="Start Time"></div><div class="col-auto"><label for="example-time-input">End Time</label><input class="form-control" type="time" value="14:45:00" id="Start Time"></div><div class="form-group"><label for="inputName4">Room</label><input type="text" class="form-control" required id = "room"></div></div><div class="form-row col-auto"><div class="form-group"><label for="inputAddress">Comments</label><textarea class="form-control" rows="3" required id="comments"></textarea></div></div><div class="form-row col-auto"><button class="btn btn-primary" type="submit">Submit form</button></div></div></div></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

//add sla page
function createAddModulePage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<button onclick="requestAdd(\'general\')" class="btn btn-default">Back</button><br><br><br><h3>Upload csv/txt file with Module details.</h3><br><br><input type="file" id="fileUpload" class="inputFileFields"><button onclick="uploadModuleFile()" class="submit">Upload</button><p id="update" style="font-size:12; color:red;"></p><br><div id="main" style="width:800px; margin:0 auto;"><form id="myForm"><div class="form-row col-auto"><div class="col-auto"><label for="inputName4">CRN</label><input type="text" class="form-control" required id = "crn"></div> <div class="col-auto"><label for="inputEmail4">School Code</label><select class="custom-select mr-sm-2" id="salutation"><option selected>Choose...</option><option value="AD">AD</option><option value="BS">BS</option><option value="HE">HE</option><option value="LW">LW</option><option value="MP">MP</option><option value="ST">ST</option></select></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputName4">Module Code</label><input type="text" class="form-control" required id = "moduleCode"></div></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Module Short Title</label><input type="text" class="form-control" required id ="moduleShortTitle"></div><div class="col-auto"><label class="control-label" for="date">Modue Long Title</label><input type="text" class="form-control" required id ="moduleLongTitle"></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputName4">Module Leader</label><input type="text" class="form-control" required id = "moduleLeader"></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Module Leaders Email</label><input type="email" class="form-control" placeholder="AB123@live.mdx.ac.uk" required id ="uniEmail"> </div></div></div><br><div class="form-row col-auto"><div class="col-auto"><button class="btn btn-primary" type="submit">Submit</button></div></div></form></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

//add sla page
function createAddEventPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<button onclick="requestAdd(\'general\')" class="btn btn-default">Back</button><br><br><br><h3>Upload csv/txt file with EVENT details.</h3><br><br><input type="file" id="fileUpload" class="inputFileFields"><button onclick="uploadSLAFile()" class="submit">Upload</button><p id="update" style="font-size:12; color:red;"></p><br> <div id="main" style="width:800px; margin:0 auto;"><form id="myForm"><div class="form-row col-auto"><div class="col-auto"><label for="inputName4">Name of the Event</label><input type="text" class="form-control" required id = "eventName"></div><div class="col-md-4"><label for="inputEmail4">Type of Event</label><select class="custom-select mr-sm-2" id="role"><option selected required>Please choose...</option><option value="Training">Training</option><option value="Observation">Observation</option><option value="Other">Other</option></select></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Tutor</label><input type="text" class="form-control" required id ="tutor"></div><div class="col-auto"><label class="control-label" for="date">Date of the Event</label><input class="form-control" id="dob" name="date" placeholder="MM/DD/YYY" type="text" required /></div></div><div class="form-row col-auto"><div class="col-auto"><label for="example-time-input">Start Time</label><input class="form-control" type="time" value="13:45:00" id="Start Time"></div><div class="col-auto"><label for="example-time-input">End Time</label><input class="form-control" type="time" value="14:45:00" id="Start Time"></div><div class="form-group"><label for="inputName4">Room</label><input type="text" class="form-control" required id = "room"></div></div><div class="form-row col-auto"><div class="form-group"><label for="inputAddress">Comments</label><textarea class="form-control" rows="3" required id="comments"></textarea></div></div><div class="form-row col-auto"><button class="btn btn-primary" type="submit">Submit form</button></div></div></div></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


function createAddLecturerPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}

  var htmlStr = '<div id="main" style="width:800px; margin:0 auto;"><form id="myForm"><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">Title</label><select class="custom-select mr-sm-2" id="salutation"><option selected>Choose...</option><option value="Mr.">Mr.</option><option value="Mrs.">Mrs.</option><option value="Ms.">Ms.</option></select></div><div class="col-auto"><label for="inputName4">First name</label><input type="text" class="form-control" placeholder="John" required id = "firstName"></div><div class="col-auto"><label for="validationServer02">Last name</label><input type="text" class="form-control" id="lastName" placeholder="Smith" required></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">University Email</label><input type="email" class="form-control" placeholder="AB123@live.mdx.ac.uk" required id ="uniEmail"></div><div class="col-md-4"><label for="inputEmail4">Area/Discipline of study</label><select class="custom-select mr-sm-2" id="department"><option selected required>Please choose...</option><option value="Art & Design">Art & Design</option><option value="Business">Business</option><option value="Health & Education">Health & Education</option><option value="Media & Performing Arts">Media & Performing Arts</option><option value="Science & Technology">Science & Technology</option></select></div></div><div class="form-row col-auto"><div class="form-group"><div class="form-row col-auto"><br><button class="btn btn-primary" type="submit" onclick="sendingLecturerDetails()">Submit</button></div></div></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

function createAddStudentPage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}

  var htmlStr = '<div id="main" style="width:800px; margin:0 auto;"><form id="myForm"><div class="form-row col-auto"><div class="col-auto"><label for="inputName4">First name</label><input type="text" class="form-control" placeholder="John" required id = "firstName"></div><div class="col-auto"><label for="validationServer02">Last name</label><input type="text" class="form-control" id="lastName" placeholder="Smith" required></div></div><div class="form-row col-auto"><div class="col-auto"><label for="inputEmail4">University Email</label><input type="email" class="form-control" placeholder="AB123@live.mdx.ac.uk" required id ="uniEmail"></div><div class="col-md-4"><label for="inputName4">Student Number</label><input type="text" class="form-control" placeholder="M00123456" required id = "studentNumber"></div> </div><br><div class="form-row col-auto"><button class="btn btn-primary" type="submit" onclick="sendingStudentDetails()">Submit</button></div></form></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

//manage page
function createManagePage() {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
  var htmlStr = '<center style="padding-top: 40px;"><h3>Manage</h3><button onclick="" class="btn-lg btn-primary danger">MAHARA</button><br><br><button onclick="requestManage(\'attendance\')" class="btn-lg btn-danger">ATTENDANCE</button><br><br><button onclick="requestManage(\'nominations\')" class="btn-lg btn-primary danger">NOMINATIONS</button><br><br><button onclick="requestManage(\'applications\')" class="btn-lg btn-primary">APPLICATIONS</button><br><br><button onclick="requestManage(\'moduleAssignment\')" class="btn-lg btn-danger">MODULE ASSIGNMENT</button></center>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


function createAttendancePage(eventArr) {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
	//sort array
	eventArr.sort(function(a, b){return new Date(a.date + " " + a.startTime).getTime() - new Date(b.date + " " + b.startTime).getTime()});
	
	//alert(JSON.stringify(eArray));
	
	var date1 = new Date();
	//change to short date
	for(var i=0; i<eventArr.length; i++) {
		if(new Date(eventArr[i].date + " " + eventArr[i].endTime) > date1) {
			eventArr.splice(i, i+1);
			//continue; 
			i-=1;
		}
	}
	
	var htmlStr = '<div class="row"><div class="col-md-12"><h3> Attendance </h3></div></div>' + '<div class="row" style="font-size: 18px; margin-top: 10px;"><div class="col-md-5"><p>Select Event: <select id="attendanceSelect" onchange="clearSlaEvent()"><option value="noSelect">Select event...</option>';
	
	for(var e=0; e<eventArr.length; e++) {
		htmlStr += '<option value="' + eventArr[e].eventID + '">' + eventArr[e].name + ' &nbsp&nbsp' + eventArr[e].date.substring(3, 6) + eventArr[e].date.substring(0, 3) + eventArr[e].date.substring(6, 10) + '</option>';
	}
	
	htmlStr += '</select></p></div></div>' + 
	'<div class="row" style="margin: 50px 15px 5px 150px; text-align: center;"><p><input type="text" placeholder="Student Number" name="eventStudentNumber" id="eventStudentNumber" style="font-size: 18px;"><button onclick="sendSLAtoEvent()" class="btn btn-primary" style="font-size: 14px; padding: 3px 5px 3px 5px;">Submit</button></p></div>' + 
	'<div class="row" style="border-top: solid 1px black;"><div class="col-md-12" id="connectSLAEvent"></div></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

function createModuleAssignmentPage(moduleArr) {
	if(document.getElementById("userSpace") == undefined) {
		createUserNavBar();
	}
	
	var htmlStr = '<div class="row"><div class="col-md-12"><h3> Module Assignment </h3></div></div>' + 
	'<div class="row" style="font-size: 18px; margin-top: 10px;"><div class="col-md-5"><p>Select Module: <input type="text" id="moduleAssignmentSelect" placeholder="Module Code" list="modules" onchange="clearSlaModule()" /><datalist id="modules">';
	
	for(var m=0; m<moduleArr.length; m++) {
		htmlStr += '<option value="' + moduleArr[m].moduleCode + ' | ' + moduleArr[m].crn + '">';
	}
	
	
	htmlStr += '</datalist>';
	
	htmlStr += '</div></div><div class="row" style="margin: 50px 15px 5px 150px; text-align: center;"><p><input type="text" placeholder="Student Number" name="moduleStudentNumber" id="moduleStudentNumber" style="font-size: 18px;"><button onclick="sendSLAtoModule()" class="btn btn-primary" style="font-size: 14px; padding: 3px 5px 3px 5px;">Submit</button></p></div>' + 
	'<div class="row" style="border-top: solid 1px black;"><div class="col-md-12" id="connectSLAModule"></div></div>';
  
  var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

//COMPLETE!!
function createNominationPageOne(lecturer) {
	lecturer = JSON.parse(lecturer);
	//alert(lecturer.lecturerID);
	
	var htmlStr = '<div class="row" style="margin: 2px 2px 2px 2px; padding: 2px 2px 2px 2px; height: 550px; overflow: scroll;"><div class="col-md-12"><div class="row" style="background-color: silver; padding: 2px 2px 2px 5px;"><h3>Lecturer Details & Aims of Participating in the SLA Programme</h3></div>' + '<form id="nominationForm1"><div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Name of Lecturer: <input type="text" id="lecturerName" class="form-control form-control-sm" style="width: 50%;" required></p></div><div class="col-md-6"><p class= "col-form-label-sm" style="width: 100%;">Role: <input type="text" id="lecturerRole" class="form-control form-control-sm" style="width: 60%;" required></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Email: <input type="text" id="lecturerEmail" class="form-control form-control-sm" style="width: 60%;" required></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">What do you hope to achieve by having SLAs in your module or programme? <textarea id="expectationOfSlaScheme" class="form-control" rows="3"></textarea></p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Which year will they be supporting?          <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="foundationYear" value="fdnYear">Foundation Year &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="firstYear" value="firstYear">First Year &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="secondYear" value="secondYear">Second Year &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="thirdYear" value="thirdYear">Third Year &nbsp&nbsp&nbsp&nbsp <br>       </p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;"> <br><br>Other: <br><input type="text" id="supportingYearOther" class="form-control form-control-sm" style="width: 70%; display: block;"></p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">When will you be using your SLAs? <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="septStart" value="septStart">September Start &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="janStart" value="janStart">January Start</p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;"> Other: <br><input type="text" id="startTimeOther" class="form-control form-control-sm" style="width: 70%; display: block;"></p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">How often are you planning to use your SLAs? <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="weekly" value="weekly">Weekly &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="fortnightly" value="fortnightly">Fortnightly &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="monthly" value="monthly">Monthly &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="duringRevisions" value="duringRevisions">During Revisions &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="onlySpecificProjects" value="onlySpecificProjects">Only For Specific Projects       </p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;"> <br><br>Other: <br><input type="text" id="slaFrequencyOther" class="form-control form-control-sm" style="width: 70%; display: block;"></p></div></div>' + '<div class="row"><div class="col-md-12" style="text-align: right;"><button onclick="saveNominationOne(\'' + lecturer.lecturerID + '\')" class="btn btn-primary">Save & Continue</button></div></div>' + '</form>' + '</div></div>'
	
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}

function createNominationPageTwo(lecturer) {
	lecturer = JSON.parse(lecturer);
	//alert(lecturer);
	
	var htmlStr = '<div class="row" style="margin: 2px 2px 2px 2px; padding: 2px 2px 2px 2px; height: 550px; overflow: scroll;"><div class="col-md-12"><div class="row" style="background-color: silver; padding: 2px 2px 2px 5px;"><h3>Programmes, Module Codes and Nominated Students</h3></div>' + '<form id="nominationForm2"><div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Please state if your SLAs will be: <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="radio" id="programmeBased" value="programmeBased" onclick="checkPM(\'programme\')">Programme Based &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="radio" id="moduleBased" value="moduleBased" onclick="checkPM(\'module\')">Module Based &nbsp&nbsp&nbsp&nbsp </div></div>' + '<div class="row" id="PMCheck"></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Comments <textarea id="nominationComments" class="form-control" rows="2"></textarea></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">We would like your support with SLA Observations. Details and support will be provided. Please state if you are available to help: <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="radio" id="nominationObservationYes" value="nominationObservationYes">Yes &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="radio" id="nominationObservationNo" value="nominationObservationNo">No &nbsp&nbsp&nbsp&nbsp </div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Thank you! <br> Kindly submit your nomination form by Friday, July 19th, 2019 by clicking on the \'Submit\' button below.</p></div></div>' + '<div class="row"><div class="col-md-12" style="text-align: right;"><button onclick="saveNominationTwo(\'' + lecturer.nominationID + '\')" class="btn btn-primary">Submit</button></div></div>' + '</form></div></div>';
	
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


function checkPM(str) {
	var htmlStr = '';
	if(str == "programme") {
		document.getElementById("moduleBased").checked = false;
		htmlStr = '<div class="col-md-12" style="border: dashed 1px silver; padding-bottom: 10px;">' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Programme Title: <input type="text" id="programmeTitle" class="form-control form-control-sm" style="width: 60%;"></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Given that 1 SLA can only work up to 6 hours per week please specify how many SLAs and/or SLA hours you require? (eg. 1 SLA x 6hrs; 1 SLA x 3hrs; 2 SLAs x4 hrs each, etc). Please note that this is subject to funding and priority of needs. <input type="text" id="slaRequirementsP" class="form-control form-control-sm" style="width: 60%;"></p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">In what learning environments will your SLAs be based? <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="workshopsP" value="workshopsP">Workshops &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="seminarsP" value="seminarsP">Seminars <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="labsP" value="labsP">Labs &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="studioP" value="studioP">Studio <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="outOfClassP" value="outOfClassP">Out of Class Sessions &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="onlineSupportP" value="onlineSupportP">Online Support</p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;"><br><br> Other: <br><input type="text" id="learningEnvironmentOtherP" class="form-control form-control-sm" style="width: 70%; display: block;"></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Please state a Core Module Code supported by the SLAs, so that they can access their CPD. <input type="text" id="programmeCoreModule" class="form-control form-control-sm" style="width: 60%;"></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%; margin: 0px 0px 0px 0px;">Please provide the details of the students you would like to nominate:</p><table id="nominationStudentsP"><tr><th></th><th>Student Number</th><th>First Name</th><th>Last Name</th><th>University Email</th><th></th></tr><tr><td id="nominationNumber1">1</td><td><input type="text" id="nominationStudentNumber1" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationFirstName1" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationLastName1" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationUniEmail1" class="form-control form-control-sm" style="width: 90%;"></td><td><button onclick="addRowForNominationStudents(\'P\')" id="nominationAddMore1" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_add.png" style="width: 20px; height: 20px;"></button></td></tr></table></div></div>' + '</div>';
	} else if(str == "module") {
		document.getElementById("programmeBased").checked = false;
		htmlStr = '<div class="col-md-12" style="border: dashed 1px silver; padding-bottom: 10px;" id="nominationModuleBased">' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">How many modules would you like to nominate for? <select id="nominationModulesSelect" class="form-control form-control-sm" onchange="createModulesForNomination()"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></p></div></div>' + '</div>';
		
		
	}
	
	var contents = document.getElementById("PMCheck");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
  
  if(str == "module") {
	  createModulesForNomination();
  }
}

function addRowForNominationStudents(str) {
	var allRows = document.getElementById("nominationStudents" + str).querySelectorAll("tr");
	
	var num = Number(allRows[allRows.length - 1].childNodes[0].innerHTML);
	
	allRows[allRows.length - 1].childNodes[allRows[allRows.length - 1].childNodes.length-1].removeChild(allRows[allRows.length - 1].childNodes[allRows[allRows.length - 1].childNodes.length-1].childNodes[0]);
	
	var newRow = document.createElement("tr");
	newRow.innerHTML = '<td id="nominationNumber1">' + (num+1) + '</td><td><input type="text" id="nominationStudentNumber' + (num+1) + '" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationFirstName' + (num+1) + '" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationLastName' + (num+1) + '" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationUniEmail' + (num+1) + '" class="form-control form-control-sm" style="width: 90%;"></td><td><button onclick="addRowForNominationStudents()" id="nominationAddMore' + (num+1) + '" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_add.png" style="width: 20px; height: 20px;"></button></td>';
	
	document.getElementById("nominationStudents" + str).childNodes[0].appendChild(newRow);
}


function createModulesForNomination() {
	var num = Number(document.getElementById("nominationModulesSelect").value);
	
	var allModules = document.getElementById("PMCheck").querySelectorAll(".nominationModule");
	
	for(var r=0; r<allModules.length; r++) {
		document.getElementById("nominationModuleBased").removeChild(allModules[r]);
	}
	
	for(var m=0; m<num; m++) {
		var newDiv = document.createElement("div");
		newDiv.className = "row nominationModule";
		newDiv.innerHTML = '<div class="col-md-12">' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Module Code ' + (m+1) + ': <input type="text" id="moduleCodeM' + (m+1) + '" class="form-control form-control-sm" style="width: 60%;"></p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Module Title ' + (m+1) + ': <input type="text" id="moduleTitleM' + (m+1) + '" class="form-control form-control-sm" style="width: 60%;"></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Given that 1 SLA can only work up to 6 hours per week please specify how many SLAs and/or SLA hours you require? (eg. 1 SLA x 6hrs; 1 SLA x 3hrs; 2 SLAs x4 hrs each, etc). Please note that this is subject to funding and priority of needs. <input type="text" id="slaRequirementsM' + (m+1) + '" class="form-control form-control-sm" style="width: 60%;"></p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">In what learning environments will your SLAs be based? <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="workshopsM' + (m+1) + '" value="workshopsM' + (m+1) + '">Workshops &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="seminarsM' + (m+1) + '" value="seminarsM' + (m+1) + '">Seminars <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="labsM' + (m+1) + '" value="labsM' + (m+1) + '">Labs &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="studioM' + (m+1) + '" value="studioM' + (m+1) + '">Studio <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="outOfClassM' + (m+1) + '" value="outOfClassM' + (m+1) + '">Out of Class Sessions &nbsp&nbsp&nbsp&nbsp <br>&nbsp&nbsp&nbsp&nbsp<input class="form-check-input" type="checkbox" id="onlineSupportM' + (m+1) + '" value="onlineSupportM' + (m+1) + '">Online Support</p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;"><br><br> Other: <br><input type="text" id="learningEnvironmentOtherM' + (m+1) + '" class="form-control form-control-sm" style="width: 70%; display: block;"></p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%; margin: 0px 0px 0px 0px;">Please provide the details of the students you would like to nominate:</p><table id="nominationStudentsM' + (m+1) + '"><tr><th></th><th>Student Number</th><th>First Name</th><th>Last Name</th><th>University Email</th><th></th></tr><tr><td id="nominationNumber1">1</td><td><input type="text" id="nominationStudentNumber1" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationFirstName1" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationLastName1" class="form-control form-control-sm" style="width: 90%;"></td><td><input type="text" id="nominationUniEmail1" class="form-control form-control-sm" style="width: 90%;"></td><td><button onclick="addRowForNominationStudents(\'M' + (m+1) + '\')" id="nominationAddMore1" style="border: none; background-color: #00000000; font-size: 16px;"><img src="images/icon_add.png" style="width: 20px; height: 20px;"></button></td></tr></table></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Comments <textarea id="nominationCommentsM' + (m+1) + '" class="form-control" rows="2"></textarea></p></div></div>' + '</div>';
		
		document.getElementById("nominationModuleBased").appendChild(newDiv);
	}
}

function createNominationPageComplete(lecturer, nomination) {
	lecturer = JSON.parse(lecturer);
	nomination = JSON.parse(nomination);
	var htmlStr = '<div class="row" style="margin: 2px 2px 2px 2px; padding: 2px 2px 2px 2px; height: 550px; overflow: scroll;"><div class="col-md-12"><div class="row" style="background-color: silver; padding: 2px 2px 2px 5px;"><h3>Lecturer Details & Aims of Participating in the SLA Programme</h3></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Name of Lecturer: ' + lecturer.firstName + ' ' + lecturer.lastName + '</p></div><div class="col-md-6"><p class= "col-form-label-sm" style="width: 100%;">Role: ' + lecturer.lecturerRole + '</p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Email: ' + lecturer.uniEmail + '</p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">What do you hope to achieve by having SLAs in your module or programme? <textarea id="expectationOfSlaScheme" class="form-control" rows="3">' + lecturer.expectation + '</textarea></p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Which year will they be supporting?          <br>' + lecturer.supportingYear + '</p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">When will you be using your SLAs? <br>' + lecturer.startTime + '</p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">How often are you planning to use your SLAs? <br> ' + lecturer.slaFrequency + '</p></div></div>' + '</div>' + '<div class="col-md-12"><div class="row" style="background-color: silver; padding: 2px 2px 2px 5px;"><h3>Programmes, Module Codes and Nominated Students</h3></div>'; 
	//alert(JSON.stringify(nomination));
	for(var i=0; i<nomination.length; i++) {
		htmlStr += '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Your SLAs will be supporting: <br> ' + nomination[i].type + '</p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Module Code ' + (i+1) + ': ' + nomination[i].moduleCode + '</p></div><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">Module Title ' + (i+1) + ': ' + nomination[i].moduleTitle + '</p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">SLA Requirements: ' + nomination[i].slaRequirements + '</p></div></div>' + '<div class="row"><div class="col-md-6"><p class="col-form-label-sm" style="width: 100%;">SLA Learning Environment:  <br>' + nomination[i].learningEnvironment + '</p></div></div>' + '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%; margin: 0px 0px 0px 0px; padding-bottom: 5px;">Students you have nominated for this module: </p></div></div>'; 
		
		//alert(nomination[i].students);
		for(var s=0; s<nomination[i].students.length; s++) {
			htmlStr += '<div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%; margin: 2px 2px 2px 2px; padding-bottom: 2px; padding-top: 0px;">' + nomination[i].students[s].studentNumber + ' &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + nomination[i].students[s].firstName + ' ' + nomination[i].students[s].lastName + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + nomination[i].students[s].uniEmail + '</p></div></div>'
		}
		
		htmlStr += '<br><div class="row"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">Comments: <br>' + nomination[i].comments + '</p></div></div>' + '<div class="row" style="border-bottom: double 1px silver;"><div class="col-md-12"><p class="col-form-label-sm" style="width: 100%;">We would like your support with SLA Observations. Details and support will be provided. Please state if you are available to help: <br> ' + nomination[i].observationHelp + '</p></div></div>';
	}
	
	htmlStr += '</div>' + '</div>';
	
	var contents = document.getElementById("changePage");
  //contents.style.height = "100%";
  contents.innerHTML = htmlStr;
}


function clearSlaEvent() { 
	document.getElementById("connectSLAEvent").innerHTML = "";	
}

function clearSlaModule() { 
	document.getElementById("connectSLAModule").innerHTML = "";	
}


    ////////////////////////
   //   page requests    //
  ////////////////////////

//COMPLETE!!!
function saveNominationTwo(nominationID) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
			requestDashboard('lecturer');
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
	
	var nomination = {};
	nomination.nominationID = nominationID;
	if(document.getElementById("programmeBased").checked) {
		nomination.type = "ProgrammeBased";
	} else {
		nomination.type = "ModuleBased";
	}
	
	var numberOfModules = 1;
	if(document.getElementById("moduleBased").checked) {
		numberOfModules = Number(document.getElementById("nominationModulesSelect").value);
	}
	nomination.numberOfModules = numberOfModules;
	
	if(nomination.type == "ProgrammeBased") {
		nomination.modules = [];
		for(var t=0; t<numberOfModules; t++) {
			var module = {};
			module.moduleCode = document.getElementById("programmeCoreModule").value;
			module.moduleTitle = document.getElementById("programmeTitle").value;
			module.slaRequirements = document.getElementById("slaRequirementsP").value;
		
			module.learningEnvironment = "";
			if(document.getElementById("workshopsP").checked) {
				module.learningEnvironment += "workshops | ";
			}
			if(document.getElementById("seminarsP").checked) {
				module.learningEnvironment += "seminars | ";
			}
			if(document.getElementById("labsP").checked) {
				module.learningEnvironment += "labs | ";
			}
			if(document.getElementById("studioP").checked) {
				module.learningEnvironment += "studio | ";
			}
			if(document.getElementById("outOfClassP").checked) {
				module.learningEnvironment += "outOfClass | ";
			}
			if(document.getElementById("onlineSupportP").checked) {
				module.learningEnvironment += "onlineSupport | ";
			}
			if(document.getElementById("learningEnvironmentOtherP").value != "") {
				module.learningEnvironment += document.getElementById("learningEnvironmentOtherP").value + " | ";
			}
			module.learningEnvironment = module.learningEnvironment.substring(0, module.learningEnvironment.length-3);
		
			module.comments = document.getElementById("nominationComments").value;
		
			if(document.getElementById("nominationObservationYes").checked) {
				nomination.observationHelp = "Yes";
			} else if(document.getElementById("nominationObservationNo").checked) {
				nomination.observationHelp = "No";
			}
		
			//nomination students
			module.students = [];
			var students = document.getElementById("nominationStudentsP").querySelectorAll("tr");
			for(var s=1; s<students.length; s++) {
				var studentDetails = students[s].querySelectorAll("td");
				var student = {};
				student.studentNumber = studentDetails[1].childNodes[0].value;
				student.firstName = studentDetails[2].childNodes[0].value;
				student.lastName = studentDetails[3].childNodes[0].value;
				student.uniEmail = studentDetails[4].childNodes[0].value;
				module.students.push(student);
			}
			nomination.modules.push(module);
		}
	} else if(nomination.type == "ModuleBased") {
		nomination.modules = [];
		for(var t=0; t<numberOfModules; t++) {
			var module = {};
			module.moduleCode = document.getElementById("moduleCodeM" + (t+1)).value;
			module.moduleTitle = document.getElementById("moduleTitleM" + (t+1)).value;
			module.slaRequirements = document.getElementById("slaRequirementsM" + (t+1)).value;
		
			module.learningEnvironment = "";
			if(document.getElementById("workshopsM" + (t+1)).checked) {
				module.learningEnvironment += "workshops | ";
			}
			if(document.getElementById("seminarsM" + (t+1)).checked) {
				module.learningEnvironment += "seminars | ";
			}
			if(document.getElementById("labsM" + (t+1)).checked) {
				module.learningEnvironment += "labs | ";
			}
			if(document.getElementById("studioM" + (t+1)).checked) {
				module.learningEnvironment += "studio | ";
			}
			if(document.getElementById("outOfClassM" + (t+1)).checked) {
				module.learningEnvironment += "outOfClass | ";
			}
			if(document.getElementById("onlineSupportM" + (t+1)).checked) {
				module.learningEnvironment += "onlineSupport | ";
			}
			if(document.getElementById("learningEnvironmentOtherM" + (t+1)).value != "") {
				module.learningEnvironment += document.getElementById("learningEnvironmentOtherM").value + " | ";
			}
			module.learningEnvironment = module.learningEnvironment.substring(0, module.learningEnvironment.length-3);
		
			module.comments = document.getElementById("nominationCommentsM" + (t+1)).value;
		
			if(document.getElementById("nominationObservationYes").checked) {
				nomination.observationHelp = "Yes";
			} else if(document.getElementById("nominationObservationNo").checked) {
				nomination.observationHelp = "No";
			}
		
			//nomination students
			module.students = [];
			var studentsArr = document.getElementById("nominationStudentsM" + (t+1)).querySelectorAll("tr");
			for(var s=1; s<studentsArr.length; s++) {
				var studentDetails = studentsArr[s].querySelectorAll("td");
				var student = {};
				student.studentNumber = studentDetails[1].childNodes[0].value;
				student.firstName = studentDetails[2].childNodes[0].value;
				student.lastName = studentDetails[3].childNodes[0].value;
				student.uniEmail = studentDetails[4].childNodes[0].value;
				module.students.push(student);
			}
			nomination.modules.push(module);
		}
	}
	
	var nominationJSON = JSON.stringify(nomination);
	
	alert(nominationJSON);
	
    xhttp.open("POST", "/nominationModule", true);
    xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send(nominationJSON);
	//requestDashboard('lecturer');
}


function saveNominationOne(lecturerID) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
			requestNomination('two');
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
	
	var nomination = {};
	nomination.lecturerID = lecturerID;
	nomination.lecturerRole = document.getElementById("lecturerRole").value;
	nomination.expectation = document.getElementById("expectationOfSlaScheme").value;
	
	nomination.supportingYear = "";
	if(document.getElementById("foundationYear").checked) {
		nomination.supportingYear += "foundationYear | ";
	}
	if(document.getElementById("firstYear").checked) {
		nomination.supportingYear += "firstYear | ";
	}
	if(document.getElementById("secondYear").checked) {
		nomination.supportingYear += "secondYear | ";
	}
	if(document.getElementById("thirdYear").checked) {
		nomination.supportingYear += "thirdYear | ";
	}
	if(document.getElementById("supportingYearOther").value != "") {
		nomination.supportingYear += document.getElementById("supportingYearOther").value + " | ";
	}
	nomination.supportingYear = nomination.supportingYear.substring(0, nomination.supportingYear.length-3);
	
	nomination.startTime = "";
	if(document.getElementById("septStart").checked) {
		nomination.startTime += "septStart | ";
	}
	if(document.getElementById("janStart").checked) {
		nomination.startTime += "janStart | ";
	}
	if(document.getElementById("startTimeOther").value != "") {
		nomination.startTime += document.getElementById("startTimeOther").value + " | ";
	}
	nomination.startTime = nomination.startTime.substring(0, nomination.startTime.length-3);
	
	nomination.slaFrequency = "";
	if(document.getElementById("weekly").checked) {
		nomination.slaFrequency += "weekly | ";
	}
	if(document.getElementById("fortnightly").checked) {
		nomination.slaFrequency += "fortnightly | ";
	}
	if(document.getElementById("monthly").checked) {
		nomination.slaFrequency += "monthly | ";
	}
	if(document.getElementById("duringRevisions").checked) {
		nomination.slaFrequency += "duringRevisions | ";
	}
	if(document.getElementById("onlySpecificProjects").checked) {
		nomination.slaFrequency += "onlySpecificProjects | ";
	}
	if(document.getElementById("slaFrequencyOther").value != "") {
		nomination.slaFrequency += document.getElementById("slaFrequencyOther").value + " | ";
	}
	nomination.slaFrequency = nomination.slaFrequency.substring(0, nomination.slaFrequency.length-3);
	
	var nominationJSON = JSON.stringify(nomination);
	
    xhttp.open("POST", "/nominationLecturer", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(nominationJSON);
    //alert(jsonstaff);
	requestNomination('two');
}


function slaToEventFeedback(str, studentNumber) {
	var strAr = str.split(",");
	
	var htmlStr = '<div class="row" style="padding-top: 10px; font-size: 14px; margin: 0px 0px 0px 0px;"><div class="col-md-5">';
	
	if(strAr[1] == " student does not exist") {
		htmlStr += '<p style="color: red; width: 100%;">' + strAr[2] + '</p></div><div class="col-md-7" style="text-align: right;"><p style="color: red; width: 100%;">ERROR</p></div>';
	} else if(strAr[0] == "duplicate") {
		htmlStr += '<p style="color: red; width: 100%;">' + strAr[1] + '</p></div><div class="col-md-7" style="text-align: right;"><p style="color: red; width: 100%;">DUPLICATE</p></div>';
	} else if(strAr[0] == "new") {
		htmlStr += '<p style="width: 100%;">' + strAr[1] + '</p></div><div class="col-md-7" style="text-align: right;"><p style="width: 100%;"><button onclick="deleteSlaEventCombo(this, \'' + studentNumber + '\')" style="border: none; background-color: #00000000; font-size: 16px;" onmouseover="this.className = \'deleteHover\';" onmouseout="this.className = \'\';">DEL</button></p></div>';
	}
		
	htmlStr += '</div>';
	return htmlStr;
}

function slaToModuleFeedback(str, studentNumber) {
	var strAr = str.split(",");
	
	var htmlStr = '<div class="row" style="padding-top: 10px; font-size: 14px;"><div class="col-md-5">';
	
	if(strAr[1] == " student does not exist") {
		htmlStr += '<p style="color: red; width: 100%;">' + strAr[2] + '</p></div><div class="col-md-7" style="text-align: right;"><p style="color: red; width: 100%;">ERROR</p></div>';
	} else if(strAr[0] == "duplicate") {
		htmlStr += '<p style="color: red; width: 100%;">' + strAr[1] + '</p></div><div class="col-md-7" style="text-align: right;"><p style="color: red; width: 100%;">DUPLICATE</p></div>';
	} else if(strAr[0] == "new") {
		htmlStr += '<p style="width: 100%;">' + strAr[1] + '</p></div><div class="col-md-7" style="text-align: right;"><p style="width: 100%;"><button onclick="deleteSlaModuleCombo(this, \'' + studentNumber + '\')" style="border: none; background-color: #00000000; font-size: 16px;" onmouseover="this.className = \'deleteHover\';" onmouseout="this.className = \'\';">DEL</button></p></div>';
	}
		
	htmlStr += '</div>';
	return htmlStr;
}

function deleteSlaEventCombo(obj, studentNumber) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  obj.parentElement.parentElement.parentElement.innerHTML = "";
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
  var eventID = document.getElementById("attendanceSelect").value;
  
    xhttp.open("GET", "/deleteSlaEvent/?studentNumber=" + studentNumber + "&eventID=" + eventID, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function deleteSlaModuleCombo(obj, studentNumber) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  obj.parentElement.parentElement.parentElement.innerHTML = "";
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
  var moduleID = document.getElementById("moduleAssignmentSelect").value.split(" | ")[1];
  
    xhttp.open("GET", "/deleteSlaModule/?studentNumber=" + studentNumber + "&moduleID=" + moduleID, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function deleteSlaEventProfile(studentNumber, eventID) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
    xhttp.open("GET", "/deleteSlaEvent/?studentNumber=" + studentNumber + "&eventID=" + eventID, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function deleteSlaModuleProfile(studentNumber, crn) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
    xhttp.open("GET", "/deleteSlaModule/?studentNumber=" + studentNumber + "&moduleID=" + crn, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}


function deleteSlaMaharaComment(studentNumber, maharaID) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
    xhttp.open("GET", "/deleteSlaMaharaComment/?studentNumber=" + studentNumber + "&maharaID=" + maharaID, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}


function sendSLAtoModule() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		document.getElementById("connectSLAModule").innerHTML += slaToModuleFeedback(this.responseText, document.getElementById("moduleStudentNumber").value);
		document.getElementById("moduleStudentNumber").value = "";
      }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
	var moduleID = document.getElementById("moduleAssignmentSelect").value;
	var sla = document.getElementById("moduleStudentNumber").value;
	
	moduleID = moduleID.split(" | ");
		
		var jsonSla = {};
		jsonSla.moduleID = moduleID[1];
		jsonSla.sla = sla;
		
		jsonSla = JSON.stringify(jsonSla);
		//alert(jsonSla);    
		xhttp.open("POST", "/moduleAssignment", true);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(jsonSla);
}

function sendSLAtoEvent() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		document.getElementById("connectSLAEvent").innerHTML += slaToEventFeedback(this.responseText, document.getElementById("eventStudentNumber").value);
		document.getElementById("eventStudentNumber").value = "";
      }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
	var eventID = document.getElementById("attendanceSelect").value;
	var sla = document.getElementById("eventStudentNumber").value;
	
	if(eventID == "noSelect") {
		alert("Please select event!")
	} else {
		
		//uploadFile();
		var jsonSla = {};
		jsonSla.eventID = eventID;
		jsonSla.sla = sla;
		
		jsonSla = JSON.stringify(jsonSla);
		//alert(jsonSla);    
		xhttp.open("POST", "/attendance", true);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(jsonSla);
	}
}


function uploadSLAs() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText);   
          resultsForUpload(JSON.parse(this.responseText));
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
  document.getElementById("update").innerHTML = "Uploading file...";
  
	//uploadFile();
    var jsonstaff = JSON.stringify(slaArray);
    //alert(jsonstaff);    
    
    xhttp.open("POST", "/upload/sla", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(jsonstaff);
    //alert(jsonstaff);
}

var moduleUploadCount = 0;
function uploadModules() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText);   
          moduleUploadCount += Number(this.responseText.substring(this.responseText.length -3, this.responseText.length));
		  
		  document.getElementById("update").innerHTML = "Uploaded " + moduleUploadCount + " modules. (Duplicates will be replaced.)";
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
  document.getElementById("update").innerHTML = "Uploading file...";
  
	//uploadFile();
    var jsonstaff = JSON.stringify(moduleArray);
    //alert(jsonstaff);    
    
    xhttp.open("POST", "/upload/module", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(jsonstaff);
    //alert(jsonstaff);
}


function loadAllSLAs() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //Convert JSON to a JavaScript object
          //alert(this.responseText);
		  var dataObj = JSON.parse(this.responseText);
		  var artDesign = document.getElementById("artDesign").checked;
		  var business = document.getElementById("business").checked;
		  var healthEducation = document.getElementById("healthEducation").checked;
		  var law = document.getElementById("law").checked;
		  var mediaPerformingArts = document.getElementById("mediaPerformingArts").checked;
		  var scienceTechnology = document.getElementById("scienceTechnology").checked;
		  filterSLA(artDesign, business, healthEducation, law, mediaPerformingArts, scienceTechnology, dataObj, "list");
      }
    };
	
	createSearchSLAPage();

    //Request data from all users
      xhttp.open("GET", "/users", true);
      xhttp.send();
}


function searchSLA(format) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
          var dataObj = JSON.parse(this.responseText);
		 // loadSLAsGivenRequest(dataObj, format);
		  var artDesign = document.getElementById("artDesign").checked;
		  var business = document.getElementById("business").checked;
		  var healthEducation = document.getElementById("healthEducation").checked;
		  var law = document.getElementById("law").checked;
		  var mediaPerformingArts = document.getElementById("mediaPerformingArts").checked;
		  var scienceTechnology = document.getElementById("scienceTechnology").checked;
		  filterSLA(artDesign, business, healthEducation, law, mediaPerformingArts, scienceTechnology, dataObj, "list");
    }
    else {
      //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  //get values from fields...
  var searchBy = document.getElementById("searchBy").value;
  var searchCriteria = document.getElementById("searchCriteria").value;
  
  //alert("searchBy=" + searchBy + "&search=" + searchCriteria)
  xhttp.open("GET", "/search/?searchBy=" + searchBy + "&search=" + searchCriteria, true);
  //xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send();
}

function filterSLA(artDesign, business, healthEducation, law, mediaPerformingArts, scienceTechnology, dataObj, format) {
	var filteredSLAs = [];

		if(artDesign == true) {
		for (var i = 0; i < dataObj.length; i++) {
		  if(dataObj[i].school == "Art & Design") {
			filteredSLAs.push(dataObj[i]);
		  }
		}
	}
		if(business == true) {
			for (var i = 0; i < dataObj.length; i++) {
			  if(dataObj[i].school == "Business") {
				filteredSLAs.push(dataObj[i]);
			  }
			}
		}
		if(healthEducation == true) {
		for (var i = 0; i < dataObj.length; i++) {
		  if(dataObj[i].school == "Health & Education") {
			filteredSLAs.push(dataObj[i]);
		    }
		}
	}
	  if(law == true) {
		for (var i = 0; i < dataObj.length; i++) {
		 if(dataObj[i].school == "Law") {
			filteredSLAs.push(dataObj[i]);
			}
		}
	}
		if(mediaPerformingArts == true) {
			for (var i = 0; i < dataObj.length; i++) {
			  if(dataObj[i].school == "Media & Performing Arts") {
				filteredSLAs.push(dataObj[i]);
				}
		}
	}
		if(scienceTechnology == true) {
			for (var i = 0; i < dataObj.length; i++) {
			  if(dataObj[i].school == "Science & Technology") {
				filteredSLAs.push(dataObj[i]);
			    }
			}
		}

		if(filteredSLAs.length == 0 && artDesign == false && business == false && healthEducation == false && law == false && mediaPerformingArts == false && scienceTechnology == false) {
			filteredSLAs = dataObj;
		}
		loadSLAsGivenRequest(filteredSLAs, format);

}


function searchModule(format) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
          var dataObj = JSON.parse(this.responseText);
          loadModulesGivenRequest(dataObj, format);
    }
    else {
      //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  //get values from fields...
  var searchBy = document.getElementById("searchBy").value;
  var searchCriteria = document.getElementById("searchCriteria").value;
  
  //alert("searchBy=" + searchBy + "&search=" + searchCriteria)
  xhttp.open("GET", "/searchModule/?searchBy=" + searchBy + "&search=" + searchCriteria, true);
  //xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send();
}

function searchEvent(format) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
          var dataObj = JSON.parse(this.responseText);
		  if(document.getElementById("searchBy").value == "date") {
			dataObj = sortEvents(dataObj);
			var dataTemp = dataObj;
			dataObj = dataTemp[1];
			if(dataTemp[0] != "no event") {
				dataObj.push(dataTemp[0]);
			}
		  }
		  //alert(JSON.stringify(dataObj));
          loadEventsGivenRequest(dataObj, format);
    }
    else {
      //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  //get values from fields...
  var searchBy = document.getElementById("searchBy").value;
  var searchCriteria = document.getElementById("searchCriteria").value;
  
  //alert("searchBy=" + searchBy + "&search=" + searchCriteria)
  xhttp.open("GET", "/searchEvent/?searchBy=" + searchBy + "&search=" + searchCriteria, true);
  //xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send();
}

function downloadSearch() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
          var dataObj = JSON.parse(this.responseText);
          JSONtoCSV(dataObj, "sla");
    }
    else {
      //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  //get values from fields...
  var searchBy = document.getElementById("searchBy").value;
  var searchCriteria = document.getElementById("searchCriteria").value;
  
  //alert("searchBy=" + searchBy + "&search=" + searchCriteria)
  xhttp.open("GET", "/search/?searchBy=" + searchBy + "&search=" + searchCriteria, true);
  //xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send();
}

function downloadModuleSearch() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
          var dataObj = JSON.parse(this.responseText);
          JSONtoCSV(dataObj, "module");
    }
    else {
      //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  //get values from fields...
  var searchBy = document.getElementById("searchBy").value;
  var searchCriteria = document.getElementById("searchCriteria").value;
  
  //alert("searchBy=" + searchBy + "&search=" + searchCriteria)
  xhttp.open("GET", "/searchModule/?searchBy=" + searchBy + "&search=" + searchCriteria, true);
  //xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send();
}



function logout(userRole) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        if(this.responseText == "logout success!") {
			
			var docBody = document.getElementsByTagName("body")[0];//Only one body
			var contents = document.getElementById("contents");
			var logoBar = document.getElementById("logoBar")
			logoBar.innerHTML = "";
			logoBar.style = "";
			contents.innerHTML = "";
			createLoginBox();
        }
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
    xhttp.open("GET", "/logout/?role=" + userRole, true);
    xhttp.send();
}

function deleteUser(user) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
			location.reload();
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
	if(!confirm("Are you sure you would like to delete this user?")) {
		return;
	} else {
		xhttp.open("GET", "/delete/?object=sla&user=" + user, true);
		xhttp.send();   
	}
}

function deleteModule(module) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
			location.reload();
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
	if(!confirm("Are you sure you would like to delete this module?")) {
		return;
	} else {
		xhttp.open("GET", "/delete/?object=module&module=" + module, true);
		xhttp.send();   
	}
}

function deleteEvent(evnt) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
			location.reload();
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
	if(!confirm("Are you sure you would like to delete this event?")) {
		return;
	} else {
		xhttp.open("GET", "/delete/?object=evnt&evnt=" + evnt, true);
		xhttp.send();   
	}
}

function maximizeUser(user, obj) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
			//alert(this.responseText);
			maximize(JSON.parse(this.responseText)[0], obj, "sla");
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
    xhttp.open("GET", "/search/?searchBy=studentNumber&search=" + user, true);
    xhttp.send();   
}

function maximizeModule(crn, obj) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
			//alert(this.responseText);
			maximize(JSON.parse(this.responseText)[0], obj, "module");
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
    xhttp.open("GET", "/searchModule/?searchBy=crn&search=" + crn, true);
    xhttp.send();   
}

function maximizeEvent(id, obj) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
			//alert(this.responseText);
			maximize(JSON.parse(this.responseText)[0], obj, "event");
      }    
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    };
	
    xhttp.open("GET", "/searchEvent/?searchBy=eventID&search=" + id, true);
    xhttp.send();   
}


function checkSession() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		//Called when data returns from server
		//alert(this.responseText);
      if (this.readyState == 4 && this.status == 200) {
        //Get a reference to the document body
          var docBody = document.getElementsByTagName("body")[0];//Only one body 
    
        //docBody.innerHTML = '';
		//if there is a session running
          if(this.responseText.substring(0, 9) != "undefined") {
            //alert(this.responseText.substring(0, 9));
            var arr = JSON.parse(this.responseText);
			//alert(JSON.stringify(arr[4]));
			//if receives check
            if(arr[0] == "/check") {
				if(document.getElementById("userSpace") == null){
					if(arr[1].role == "Student" || arr[1].role == "SLA") {
						createStudentSLANavBar(arr[1]);
					} else if(arr[1].role == "Lecturer") {
						createLecturerNavBar(arr[1]);
					} else if(arr[1].role == "Admin" || arr[1].role == "PAL" || arr[1].role == "SSLA" || arr[1].role == "SSLA+") {
						createUserNavBar(arr[1]);
					}
				}
              getCurrentPage(arr[4], arr[1], arr[2], arr[3], arr[5]);
			  //createDashboard(arr[1], arr[2], arr[3]);
            }
			
          }
		  //if undefined, login
		  else if(this.responseText.substring(9, 11) == "21") {
            createLoginBox();
			document.getElementById("loginResult").innerHTML = "Please enter username.";
          }
		  else if(this.responseText.substring(9, 11) == "16") {
            createLoginBox();
			document.getElementById("loginResult").innerHTML = "Please enter password.";
          }
          else if(this.responseText.substring(9, 10) != "0") {
            createLoginBox();
			document.getElementById("loginResult").innerHTML = "Login Failed. Try Again.";
          }
		  
		  else {
			  createLoginBox();
		  }
      }  
      else {
        //alert("Error communicating with server: " + xhttp.status);
      }
    }; 
  //Request data from all users
    xhttp.open("GET", "/check", true);
    xhttp.send();  
}

function requestSearch(str) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload();
		if(this.responseText == "sla") {
          createSearchSLAPage();
        } else if(this.responseText == "event") {
			createSearchEventPage();
		} else if(this.responseText == "module") {
			createSearchModulePage();
		} else if(this.responseText == "add") {
			createSearchPage();
		}
	  }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
    xhttp.open("GET", "/searchPage/?page=" + str, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function requestAdd(str) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload();
		if(this.responseText == "sla") {
          createAddSLAPage();
        } else if(this.responseText == "event") {
			createAddEventPage();
		} else if(this.responseText == "staff") {
			createAddStaffPage();
		} else if(this.responseText == "lecturer") {
			createAddLecturerPage();
		} else if(this.responseText == "student") {
			createAddStudentPage();
		} 
	  }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
    xhttp.open("GET", "/add/?page=" + str, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function sendingLecturerDetails() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload(); 
	  } else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  };

	var title = document.getElementById("salutation").value;
	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var uniEmail = document.getElementById("uniEmail").value;
	var areaOfStudy = document.getElementById("department").value;
	var password = makePasword(6) ;
	
	var lecturerProfile = {};
	lecturerProfile.title = title;
	lecturerProfile.firstName = firstName;
	lecturerProfile.lastName = lastName;
	lecturerProfile.uniEmail = uniEmail;
	lecturerProfile.areaOfStudy = areaOfStudy;
	lecturerProfile.password = password;
	lecturerProfile.encPW = encPW = uniEmail.substring(0, 2) + CryptoJS.SHA3(password);

	var lecturerProfile2 = JSON.stringify(lecturerProfile);

	xhttp.open("POST", "/addlecturer", true);
	alert(lecturerProfile2);
	xhttp.setRequestHeader("Content-type", "application/json");	
	xhttp.send(lecturerProfile2);

}

function sendingModuleDetails() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload(); 
	  } else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  };

	var crn = document.getElementById("crn").value;
	var moduleCode = document.getElementById("moduleCode").value;
	var moduleShortTitle = document.getElementById("moduleShortTitle").value;
	var moduleLongTitle = document.getElementById("moduleLongTitle").value;
	var moduleLeaderEmail = document.getElementById("moduleLeaderEmail").value;

	var moduleProfile = {};
	moduleProfile.crn = crn;
	moduleProfile.moduleShortTitle = moduleCode;
	moduleProfile.moduleLongTitle = moduleShortTitle;
	moduleProfile.moduleLongTitle = moduleLongTitle;
	moduleProfile.moduleLeaderEmail = moduleLeaderEmail;
	
	var moduleProfile2 = JSON.stringify(moduleProfile);

	xhttp.open("POST", "/addmodule", true);
	alert(moduleProfile2);
	xhttp.setRequestHeader("Content-type", "application/json");	
	xhttp.send(moduleProfile2);

}

function sendingStudentDetails() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload(); 
	  } else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  };
	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var uniEmail = document.getElementById("uniEmail").value;
	var studentNumber = document.getElementById("studentNumber").value;
	var password = makePasword(6) ;
	
	var studentProfile = {};
	studentProfile.firstName = firstName;
	studentProfile.lastName = lastName;
	studentProfile.uniEmail = uniEmail;
	studentProfile.studentNumber = studentNumber;
	studentProfile.password = password;
	studentProfile.encPW = encPW = uniEmail.substring(0, 2) + CryptoJS.SHA3(password);

	var studentProfile2 = JSON.stringify(studentProfile);

	xhttp.open("POST", "/addstudent", true);
	alert(studentProfile2);
	xhttp.setRequestHeader("Content-type", "application/json");	
	xhttp.send(studentProfile2);

}

function sendingSLADetails() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload(); 
	  } else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  };
  var salutation = document.getElementById("salutation").value;
  var firstName = document.getElementById("firstName").value;
  var lastName = document.getElementById("lastName").value;
  var dob = document.getElementById("dob").value;
  var studentNumber = document.getElementById("studentNumber").value;
  var uniEmail = document.getElementById("uniEmail").value;
  var personalEmail = document.getElementById("email").value;
  var mobileNumber = document.getElementById("phoneNumber").value;
  var disabled = document.getElementById("diabled?").value;
  var disabilityType = document.getElementById("disabilityType").value;
  var programmeOfStudy = document.getElementById("course").value;
  var yearOfStudy = document.getElementById("year").value;
  var foundationYear = document.getElementById("foundation").value;
  var ITspreadsheet = document.getElementById("ITskill1").value;
  var ITdatabaseDevelopment = document.getElementById("ITskill2").value;
  var ITwebDesign = document.getElementById("ITskill3").value;
  var ITgraphics = document.getElementById("ITskill4").value;
  var ITwebAuthoring = document.getElementById("ITskill5").value;
  var IThtml = document.getElementById("ITskill6").value;
  var ITothers = document.getElementById("ITskills").value;
  var otherSkillsAcademicWriting = document.getElementById("otherSkills1").value;
  var otherSkillsCopyWriting =  document.getElementById("otherSkills2").value;
  var otherSkillsPhotography =  document.getElementById("otherSkills3").value;
  var otherSkillsVideography =  document.getElementById("otherSkills4").value;
  var otherSkillsGraphics =  document.getElementById("otherSkills5").value;
  var otherSkillsVisualContent =  document.getElementById("otherSkills6").value;
  var otherSkillsCreativeSoftware =  document.getElementById("otherSkills7").value;
  var otherSkillsMaths =  document.getElementById("otherSkills8").value;
  var otherSkillsOthers =  document.getElementById("otherSkills").value;
  var slaExperienceYes =  document.getElementById("slaExpYes").value;
  var slaExperienceNo =  document.getElementById("slaExpNo").value;
  var module1 =  document.getElementById("SlaMod1").value;
  var module2 =  document.getElementById("SlaMod2").value;
  var module3 =  document.getElementById("SlaMod3").value;
  var module4 =  document.getElementById("SlaMod4").value;
  var slaSkills =  document.getElementById("slaSkills").value;
  var slaSuccessFormula =  document.getElementById("slaSuccess").value;
	
  var slaExperience = true;
  if (document.getElementById("slaExpNo").checked) {
      slaExperience = false;
  } 

  var ITSkills1 = false;
  if(document.getElementById("ITskill1").checked) {
    ITSkills1 = true;
  }

  var ITSkills2 = false;
  if(document.getElementById("ITskill2").checked) {
    ITSkills2 = true;
  }

  var ITSkills3 = false;
  if(document.getElementById("ITskill3").checked) {
    ITSkills3 = true;
  }

  var ITskill4 = false;
  if(document.getElementById("ITskill4").checked) {
    ITskill4 = true;
  }

  var ITskill5 = false;
  if(document.getElementById("ITskill5").checked) {
    ITskill5 = true;
  }

  var ITskill6 = false;
  if(document.getElementById("ITskill6").checked) {
    ITskill6 = true;
  }
  var otherSkills1 = false;
  if(document.getElementById("otherSkills1").checked) {
    otherSkills1 = true;
  }

  var otherSkills2 = false;
  if(document.getElementById("otherSkills2").checked) {
    otherSkills2 = true;
  }

  var otherSkills3 = false;
  if(document.getElementById("otherSkills3").checked) {
    otherSkills3 = true;
  }

  var otherSkills4 = false;
  if(document.getElementById("otherSkills4").checked) {
    otherSkills4 = true;
  }

  var otherSkills5 = false;
  if(document.getElementById("otherSkills5").checked) {
    otherSkills5 = true;
  }

  var otherSkills6 = false;
  if(document.getElementById("otherSkills6").checked) {
    otherSkills6 = true;
  }

  var otherSkills7 = false;
  if(document.getElementById("otherSkills7").checked) {
    otherSkills7 = true;
  }

  var otherSkills8 = false;
  if(document.getElementById("otherSkills8").checked) {
    otherSkills8 = true;
  }
	/*var slaProfile = {};
	slaProfile.salutation = salutation;
	slaProfile.firstName = firstName;
	slaProfile.lastName = lastName;
	slaProfile.DOB = dob;
	sla
	slaProfile.uniEmail = uniEmail;
	slaProfile.studentNumber = studentNumber;*/

	var slaProfile = {title: salutation, first: firstName, last: lastName, dateOfBirth: dob, studentID: studentNumber, 
		emailUni: uniEmail, emailPersonal: personalEmail, phone: mobileNumber, slaExp: slaExperience, 
		disability: disabled, disabledType: disabilityType, skillsSla: slaSkills, successFormula: slaSuccessFormula, ITspreadsheet: ITSkills1, ITdatabaseDevelopment: ITSkills2, ITwebDesign: ITSkills3, ITgraphics: ITskill4, IThtml: ITskill5, ITWebAuthouring: ITskill6, otherSkillsAcademicWriting: otherSkills1, otherSkillsCopyWriting: otherSkills2, otherSkillsPhotography: otherSkills3,
		otherSkillsVideography: otherSkills4, otherSkillsGraphics: otherSkills5, otherSkillsVisualContent: otherSkills6, otherSkillsCreativeSoftware: otherSkills7, otherSkillsMaths: otherSkills8,
		otherSkills9: otherSkillsOthers, degree: programmeOfStudy, year: yearOfStudy, foundationYear: foundationYear, disability: disabled, disabledType: disabilityType, skillsSla: slaSkills, successFormula: slaSuccessFormula,
		ITskills1: ITspreadsheet, ITskills2: ITdatabaseDevelopment, ITskills3: ITwebDesign, ITskills4: IThtml, ITskills5: ITothers, otherSkills1: otherSkillsAcademicWriting, otherSkills2: otherSkillsCopyWriting, otherSkills3: otherSkillsPhotography,
		otherSkills4: otherSkillsVideography, otherSkills5: otherSkillsGraphics, otherSkills6: otherSkillsVisualContent, otherSkills7: otherSkillsCreativeSoftware, otherSkills8: otherSkillsMaths,
		otherSkills9: otherSkillsOthers}
	
	var slaProfile2 = JSON.stringify(slaProfile);

	xhttp.open("POST", "/addsla", true);
	alert(slaProfile2);
	xhttp.setRequestHeader("Content-type", "application/json");	
	xhttp.send(slaProfile2);

}

function requestManage(str) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload();
		if(this.responseText == "manage") {
          createManagePage();
        } else if(this.responseText == "attendance") {
			createAttendancePage();
		} else if(this.responseText == "moduleAssignment") {
			getModules();
			//createModuleAssignmentPage();
		}
	  }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
    xhttp.open("GET", "/manage/?page=" + str, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function requestNomination(str) {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        alert("xhttp.responseText = " + xhttp.responseText); 
		var obj = this.responseText.split(" || ");
		//alert(obj[0] + "      " + obj[1]);
		if(obj[0] == "nomination/one") {
          createNominationPageOne(obj[1]);
        } else if(obj[0] == "nomination/two") {
          createNominationPageTwo(obj[1]);
        } else if(obj[0] == "nomination/three") {
			createNominationPageComplete(obj[1], obj[2]);
		}
	  }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
    xhttp.open("GET", "/nomination/?page=" + str, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}

function getModules() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
          var dataObj = JSON.parse(this.responseText);
		  createModuleAssignmentPage(dataObj);
    }
    else {
      //alert("Error communicating with server: " + xhttp.status);
    }
  };
  
  //alert("searchBy=" + searchBy + "&search=" + searchCriteria)
  xhttp.open("GET", "/get/modules", true);
  //xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send();
}

function requestDashboard() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText); 
		location.reload();
      }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
  
    xhttp.open("GET", "/dashboard", true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff);
}



function loginStaff() {
  //Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
		  //alert(this.responseText);
        if(this.responseText != "login failed") {
          var staff = JSON.parse(this.responseText);
          //alert("login successful!");
          checkSession();
        } 		
      }
      else {
		//alert("Error communicating with server: " + xhttp.status);
      }
    }; 
	var encPW = "";
    var username = document.getElementById("logInUserName").value.toUpperCase();
    var password = document.getElementById("logInPassWord").value;
	if(password != "") {
		encPW = username.substring(0, 2) + CryptoJS.SHA3(password);
	}
	
	var currentDate = formatDate();
	
    xhttp.open("GET", "/login/?username=" + username + "&password=" + encPW + "&clogintime=" + currentDate, true);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    //alert(jsonstaff); 
}


function saveSLA(studentNumber, obj) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText);
		searchSLA("list");
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
	
	var allInput = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input");
	var allTextArea = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("textarea");
	
	var sla = {};
	
	if(profileTab == "profile") {
		sla.title = allInput[0].value;
		sla.firstName = allInput[1].value;
		sla.lastName = allInput[2].value;
		sla.dateOfBirth = allInput[3].value;
		sla.studentNumber = allInput[4].value;
		sla.uniEmail = allInput[5].value;
		sla.mobileNumber = allInput[6].value;
		sla.personalEmail = allInput[7].value;
	} else if(profileTab == "education") {
		sla.programme = allInput[0].value;
		sla.year = allInput[1].value;
		sla.school = allInput[2].value;
	} else if(profileTab == "application") {
		sla.whySLA = allTextArea[0].value;
		sla.successFormula = allTextArea[1].value;
		sla.previousModule1 = allInput[0].value;
		sla.previousModule2 = allInput[1].value;
		sla.previousModule3 = allInput[2].value;
		sla.previousModule4 = allInput[3].value;
	} else if(profileTab == "other") {
		sla.disabilityState = allInput[0].value;
		sla.disabilityNature = allInput[1].value;
	}
	
    var jsonstaff = JSON.stringify(sla);
    //alert(jsonstaff);    
    
    xhttp.open("POST", "/save/?tab=" + profileTab + "&studentNumber=" + studentNumber, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(jsonstaff);
    //alert(jsonstaff);
}

function saveModule(crn, obj) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText);
		searchModule("list");
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
	
	var allInput = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input");
	var allTextArea = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("textarea");
	
	var module = {};
	
	if(profileTab == "moduleGen") {
		module.crn = allInput[0].value;
		module.moduleCode = allInput[1].value;
		module.moduleShortTitle = allInput[2].value;
		module.moduleLongTitle = allInput[3].value;
		module.moduleLeaderName = allInput[4].value;
		module.moduleLeaderEmail = allInput[5].value;
	}
	
    var jsonstaff = JSON.stringify(module);
    //alert(jsonstaff);    
    
    xhttp.open("POST", "/saveModule/?tab=" + profileTab + "&crn=" + crn, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(jsonstaff);
    //alert(jsonstaff);
}

function saveEvent(ev, obj) {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText);
		searchEvent("list");
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
	
	var allInput = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input");
	var allTextArea = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("textarea");
	
	var eventObj = {};
	
	if(profileTab == "eventInfo") {
		eventObj.name = allInput[0].value;
		eventObj.type = allInput[1].value;
		eventObj.date = allInput[2].value;
		eventObj.startDate = allInput[3].value;
		eventObj.endDate = allInput[4].value;
		eventObj.tutor = allInput[5].value;
		eventObj.room = allInput[6].value;
		eventObj.comment = allInput[7].value;
	}
	
    var jsonstaff = JSON.stringify(eventObj);
    //alert(jsonstaff);    
    
    xhttp.open("POST", "/saveEvent/?tab=" + profileTab + "&eventID=" + ev, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(jsonstaff);
    //alert(jsonstaff);
}

function saveMaharaComment() {
	//Set up XMLHttpRequest
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {//Called when data returns from server
      if (this.readyState == 4 && this.status == 200) {
        //alert("xhttp.responseText = " + xhttp.responseText);
		searchSLA("list");
        }
  else {
    //alert("Error communicating with server: " + xhttp.status);
    }
  }; 
	
	var mahara = {};
	mahara.rating = Number(document.getElementById("maharaRatingSelect").value);
	mahara.maharaID = document.getElementById("maharaSelect").value;
	mahara.comment = document.getElementById("maharaComment").value;
	mahara.dateTime = formatDate();
	
	
    var jsonstaff = JSON.stringify(mahara);
    alert(jsonstaff);    
    
    xhttp.open("POST", "/saveMaharaComment", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(jsonstaff);
    //alert(jsonstaff);
}




    ////////////////////////
   //     functions      //
  ////////////////////////

function getCurrentPage(page, dash1, dash2, dash3, dash4) {
	if(page == "dashboard/staff") {
		createStaffDashboard(dash1, dash2, dash3, dash4);
	} else if(page == "dashboard/student") {
		createStudentDashboard(dash1);
	} else if(page == "dashboard/sla") {
		createSlaDashboard(dash1);
	} else if(page == "dashboard/lecturer") {
		createLecturerDashboard(dash1);
	} else if(page == "add/module") {
		createAddModulePage();
	} else if(page == "add/staff") {
		createAddStaffPage();
	} else if(page == "add/sla") {
		createAddSLAPage();
	} else if(page == "add/event") {
		createAddEventPage();
	} else if(page == "add/lecturer") {
		createAddLecturerPage();
	} else if(page == "add/student") {
		createAddStudentPage();
	} else if(page == "add") {
		createAddPage();
	} else if(page == "search") {
		createSearchPage();
	} else if(page == "search/sla") {
		createSearchSLAPage();
	} else if(page == "search/event") {
		createSearchEventPage();
	} else if(page == "search/module") {
		createSearchModulePage();
	} else if(page == "manage") {
		createManagePage();
	} else if(page == "manage/attendance") {
		createAttendancePage(dash2);
	} else if(page == "manage/nominations") {
		requestAllNominations();
	} else if(page == "manage/moduleAssignment") {
		getModules();
	} else if(page == "nomination/one") {
		requestNomination('one');
	} else if(page == "nomination/two") {
		requestNomination('two');
	} else if(page == "nomination/three") {
		requestNomination('three');
	}
}


function makePasword(length) {
	var result           = '';
	var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for ( var i = 0; i < length; i++ ) {
	   result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
 }


function resultsForUpload(uploadObj) {
	var p = document.getElementById("update");
	 var updatedCount = uploadObj.afterCount - uploadObj.beforeCount;
	p.innerHTML = "Updated " + updatedCount + " records. There were " + (uploadObj.fileCount - updatedCount + " duplicates.");
} 

function JSONtoCSV(JSONData, clsf) {
	//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
	if(clsf == "sla") {
	for(var rep = 0; rep<JSONData.length; rep++) {
		JSONData[rep].whySLA = JSONData[rep].whySLA.replace(/,/g, "");
		JSONData[rep].successFormula = JSONData[rep].successFormula.replace(/,/g, "");
	}
	}
	var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

	var CSV = '';
	var row = "";

    //This loop will extract the label from 1st index of on array
    for (var index in arrData[0]) {
		row += index + ',';
    }
	row = row.slice(0, -1);

    CSV += row + '\r\n'; //append Label row with line break

	//1st loop is to extract each row
	for (var i = 0; i < arrData.length; i++) {
		var row = "";

		//2nd loop will extract each column and convert it in string comma-seprated
		for (var index in arrData[i]) {
			row += '"' + arrData[i][index] + '",';
		}

		row.slice(0, row.length - 1);
		CSV += row + '\r\n'; //add a line break after each row
	}

	if (CSV == '') {
		alert("Invalid data");
		return;
	}

	//Generate a file name
	var fileName = "MyReport_SearchResults";

	//Initialize file format you want csv or xls
	var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

  
	//this will generate a temp <a /> tag
	var link = document.createElement("a");
	link.href = uri;

	//set the visibility hidden so it will not effect on your web-layout
	link.style = "visibility:hidden";
	link.download = fileName + ".csv";

	//this part will append the anchor tag and remove it after automatic click
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
}

function loadSLAsGivenRequest(dataObj, format) {
	var resultCount = document.getElementById("searchResultCount")
	if(dataObj.length == 1) {
		resultCount.innerHTML = " (" + dataObj.length + " Result)";
	} else {
		resultCount.innerHTML = " (" + dataObj.length + " Results)";
	}
	
  //create the SLA div
    var htmlStr = "";
	
	if(format == "list") {
	for(var l=0; l<dataObj.length; l++) {
		htmlStr += '<div class="row" style="width:100%;"><div class="col-md-12" id="shortProfileList" style="border: solid 2px black; background-color:';
		if(dataObj[l].withdrawn == "Yes") {
			htmlStr += '#ff000033';
		} else {
			htmlStr += '#ffffff';
		}
		htmlStr += '; text-align: left; margin: 2px 2px 2px 2px; font-size: 16px; padding-top: 2px;"><div class="row"><div class="col-md-1"><img src="' + selectShortProfileImage(dataObj[l]) + '" style="width:16px; height: 20px;"></div><div class="col-md-5">' + dataObj[l].firstName + ' ' + dataObj[l].lastName + '</div>'
		+ '<div class="col-md-6"><div class="row" style="text-align: right; margin: 0px 0px 2px 0px;"><p style="width: 100%; margin: 0px 0px 0px 0px;">' + school2Code(dataObj[l].school)  + ' | <button class="editButton" onclick="editSLA(\''+ dataObj[l].studentNumber +'\', this)" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_edit.png"  style="width: 100%; height: 100%;"></button><button class="minMaxButton" onclick="maximizeUser(\'' + dataObj[l].studentNumber + '\', this)" style="border: none; background-color: #00000000; width: 15px; height: 15px;"><img src="images/icon_maximize.png"  style="width: 100%; height: 100%;"></button><button class="deleteButton" onclick="deleteUser(\'' + dataObj[l].studentNumber + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;"><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></p></div></div></div></div></div>';
	}
	} else if(format == "grid") {
	for(var r=0; r<dataObj.length; r++) {
		htmlStr += '<div class="row" style="width:100%;">';
		
		for(var c=0; c<2; c++) {
			if(r >= dataObj.length){
				htmlStr += '<div class="col-md-5"></div>';
			} else {
				htmlStr += '<div class="col-md-5" id="shortProfileGrid" style="border: double 4px black; background-color:';
				if(dataObj[r].withdrawn == "Yes") {
					htmlStr += '#ff000033';
				} else {
					htmlStr += '#ffffff';
				}
				htmlStr += '; text-align: center; margin: 2px 2px 2px 2px;"><div class="row"><div class="col-md-3" style="padding: 2px 2px 2px 2px;"><div class="row" style="margin: 0px 0px 0px 0px;"><p style="width:100%;"><img id="shortProfileImage" src="' + selectShortProfileImage(dataObj[r]) + '" style="width:45px; height: 55px;"></p></div><div class="row" style="margin: 0px 0px 0px 0px;"><p style="width:100%; margin: 5px 0px 5px 0px;"><span id="shortProfileSchool">' + school2Code(dataObj[r].school) + '</span> | <span id="shortProfileSlaType">' + status3Code(dataObj[r].returningSLA, dataObj[r].seniorSLA) + '</span></p></div></div><div class="col-md-9" style="padding: 2px 5px 2px 10px; text-align:left; font-size:16px;"><div class="row" style="text-align: right; margin: 0px 0px 10px 0px;"><p style="width: 100%; margin: 0px 0px 0px 0px;"><button class="editButton" onclick="editSLA(\''+ dataObj[r].studentNumber +'\', this)" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;" disabled><img src="images/icon_edit.png"  style="width: 100%; height: 100%;"></button><button class="minMaxButton" onclick="maximizeUser(\'' + dataObj[r].studentNumber + '\', this)" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;"><img src="images/icon_maximize.png"  style="width: 100%; height: 100%;"></button><button class="deleteButton" onclick="deleteUser(\'' + dataObj[r].studentNumber + '\')" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;"><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></p></div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].firstName + ' ' + dataObj[r].lastName +'</div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].studentNumber +'</div><div class="row" style="margin: 2px 0px 2px 0px;">' + checkStatus(dataObj[r].status, dataObj[r].withdrawn) + '</div></div></div></div>';
			}
			if(c < 1) {
				htmlStr += '<div class="col-md-2" style="margin: -10px -10px -10px -10px;"></div>';
				r+=1;
			}
		}
		
		htmlStr += '</div><div class="row">&nbsp</div>';
	}
	}
    
  //append to contents          
    var contents = document.getElementById("displayResults");
    contents.innerHTML = htmlStr;
}

function loadModulesGivenRequest(dataObj, format) {
	var resultCount = document.getElementById("searchResultCount")
	if(dataObj.length == 1) {
		resultCount.innerHTML = " (" + dataObj.length + " Result)";
	} else {
		resultCount.innerHTML = " (" + dataObj.length + " Results)";
	}
	
  //create the SLA div
    var htmlStr = "";
	
	if(format == "list") {
	for(var l=0; l<dataObj.length; l++) {
		htmlStr += '<div class="row" style="width:100%;"><div class="col-md-12" id="shortProfileList" style="border: solid 2px black; background-color:#ffffff; text-align: left; margin: 2px 2px 2px 2px; font-size: 16px; padding-top: 2px;"><div class="row"><div class="col-md-1"><img src="' + selectImageFromModuleSchool(dataObj[l].schoolCode) + '" style="width:16px; height: 20px;"></div><div class="col-md-7">' + dataObj[l].moduleCode + ' | ' + dataObj[l].moduleShortTitle.replace(/"/g, "") + '</div>'
		+ '<div class="col-md-4"><div class="row" style="text-align: right; margin: 0px 0px 2px 0px;"><p style="width: 100%; margin: 0px 0px 0px 0px;">' + dataObj[l].crn  + ' | <button class="editButton" onclick="editModule(\''+ dataObj[l].crn +'\', this)" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_edit.png"  style="width: 100%; height: 100%;"></button><button class="minMaxButton" onclick="maximizeModule(\'' + dataObj[l].crn + '\', this)" style="border: none; background-color: #00000000; width: 15px; height: 15px;"><img src="images/icon_maximize.png"  style="width: 100%; height: 100%;"></button><button class="deleteButton" onclick="deleteModule(\'' + dataObj[l].crn + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;"><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></p></div></div></div></div></div>';
	}
	} else if(format == "grid") {
	for(var r=0; r<dataObj.length; r++) {
		htmlStr += '<div class="row" style="width:100%;">';
		
		for(var c=0; c<2; c++) {
			if(r >= dataObj.length){
				htmlStr += '<div class="col-md-5"></div>';
			} else {
				htmlStr += '<div class="col-md-5" id="shortProfileGrid" style="border: double 4px black; background-color:#ffffff; text-align: center; margin: 2px 2px 2px 2px;"><div class="row"><div class="col-md-3" style="padding: 2px 2px 2px 2px;"><div class="row" style="margin: 0px 0px 0px 0px;"><p style="width:100%;"><img id="shortProfileImage" src="' + selectImageFromModuleSchool(dataObj[r].schoolCode) + '" style="width:45px; height: 55px;"></p></div><div class="row" style="margin: 0px 0px 0px 0px;"><p style="width:100%; margin: 5px 0px 5px 0px;"><span id="shortProfileSchool">' + dataObj[r].schoolCode + '</span></p></div></div><div class="col-md-9" style="padding: 2px 5px 2px 10px; text-align:left; font-size:16px;"><div class="row" style="text-align: right; margin: 0px 0px 10px 0px;"><p style="width: 100%; margin: 0px 0px 0px 0px;"><button class="editButton" onclick="editModule(\''+ dataObj[r].crn +'\', this)" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;" disabled><img src="images/icon_edit.png"  style="width: 100%; height: 100%;"></button><button class="minMaxButton" onclick="maximizeModule(\'' + dataObj[r].crn + '\', this)" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;"><img src="images/icon_maximize.png"  style="width: 100%; height: 100%;"></button><button class="deleteButton" onclick="deleteModule(\'' + dataObj[r].crn + '\')" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;"><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></p></div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].crn + '</div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].moduleCode +'</div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].moduleShortTitle.replace(/"/g, "") + '</div></div></div></div>';
			}
			if(c < 1) {
				htmlStr += '<div class="col-md-2" style="margin: -10px -10px -10px -10px;"></div>';
				r+=1;
			}
		}
		
		htmlStr += '</div><div class="row">&nbsp</div>';
	}
	}
    
  //append to contents          
    var contents = document.getElementById("displayResults");
    contents.innerHTML = htmlStr;
}


function loadEventsGivenRequest(dataObj, format) {
	//alert(JSON.stringify(dataObj));
	var resultCount = document.getElementById("searchResultCount")
	if(dataObj.length == 1) {
		resultCount.innerHTML = " (" + dataObj.length + " Result)";
	} else {
		resultCount.innerHTML = " (" + dataObj.length + " Results)";
	}
	
  //create the SLA div
    var htmlStr = "";
	
	if(format == "list") {
	for(var l=0; l<dataObj.length; l++) {
		htmlStr += '<div class="row" style="width:100%;"><div class="col-md-12" id="shortProfileList" style="border: solid 2px black; background-color:#ffffff; text-align: left; margin: 2px 2px 2px 2px; font-size: 16px; padding-top: 2px;"><div class="row"><div class="col-md-1"><img src="images/icon_event.png" style="width:16px; height: 20px;"></div><div class="col-md-7">' + dataObj[l].name + ' | ' + dataObj[l].type + '</div>'
		+ '<div class="col-md-4"><div class="row" style="text-align: right; margin: 0px 0px 2px 0px;"><p style="width: 100%; margin: 0px 0px 0px 0px;"><button class="editButton" onclick="editEvent(\''+ dataObj[l].eventID +'\', this)" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_edit.png"  style="width: 100%; height: 100%;"></button><button class="minMaxButton" onclick="maximizeEvent(\'' + dataObj[l].eventID + '\', this)" style="border: none; background-color: #00000000; width: 15px; height: 15px;"><img src="images/icon_maximize.png"  style="width: 100%; height: 100%;"></button><button class="deleteButton" onclick="deleteEvent(\'' + dataObj[l].eventID + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;"><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></p></div></div></div></div></div>';
	}
	} else if(format == "grid") {
	for(var r=0; r<dataObj.length; r++) {
		htmlStr += '<div class="row" style="width:100%;">';
		
		for(var c=0; c<2; c++) {
			if(r >= dataObj.length){
				htmlStr += '<div class="col-md-5"></div>';
			} else {
				htmlStr += '<div class="col-md-5" id="shortProfileGrid" style="border: double 4px black; background-color:#ffffff; text-align: center; margin: 2px 2px 2px 2px;"><div class="row"><div class="col-md-3" style="padding: 2px 2px 2px 2px;"><div class="row" style="margin: 0px 0px 0px 0px;"><p style="width:100%;"><img id="shortProfileImage" src="images/icon_event.png" style="width:45px; height: 55px;"></p></div><div class="row" style="margin: 0px 0px 0px 0px;"><p style="width:100%; margin: 5px 0px 5px 0px;"><span id="shortProfileSchool"></span></p></div></div><div class="col-md-9" style="padding: 2px 5px 2px 10px; text-align:left; font-size:16px;"><div class="row" style="text-align: right; margin: 0px 0px 10px 0px;"><p style="width: 100%; margin: 0px 0px 0px 0px;"><button class="editButton" onclick="editEvent(\''+ dataObj[r].eventID +'\', this)" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;" disabled><img src="images/icon_edit.png"  style="width: 100%; height: 100%;"></button><button class="minMaxButton" onclick="maximizeEvent(\'' + dataObj[r].eventID + '\', this)" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;"><img src="images/icon_maximize.png"  style="width: 100%; height: 100%;"></button><button class="deleteButton" onclick="deleteEvent(\'' + dataObj[r].eventID + '\')" style="border: solid 1px silver; background-color: #00000000; width: 20px; height: 20px;"><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></p></div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].name + '</div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].type +'</div><div class="row" style="margin: 2px 0px 2px 0px;">' + dataObj[r].date + '</div></div></div></div>';
			}
			if(c < 1) {
				htmlStr += '<div class="col-md-2" style="margin: -10px -10px -10px -10px;"></div>';
				r+=1;
			}
		}
		
		htmlStr += '</div><div class="row">&nbsp</div>';
	}
	}
    
  //append to contents          
    var contents = document.getElementById("displayResults");
    contents.innerHTML = htmlStr;
}


function showModuleTab(crn, moduleCode, moduleShortTitle, moduleLongTitle, moduleLeaderName, moduleLeaderEmail) {
	profileTab = "moduleGen";
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;">' + '<div class="row"><div class="col-md-2"><p>CRN: <input type="text" style="width: 100%;" value="' + crn + '" readonly></p></div><div class="col-md-3"><p>Code: <input type="text" style="width: 90%;" value="' + moduleCode + '" readonly></p></div><div class="col-md-7"><p>Title: <input type="text" style="width: 100%;" value="' + moduleShortTitle + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-12"><p>Module Long Title: <input type="text" style="width: 80%;" value="' + moduleLongTitle + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-4"><p>Module Leader: <input type="text" style="width: 100%;" value="' + moduleLeaderName + '" readonly></p></div><div class="col-md-8"><p>Module Leader Email: <input type="text" style="width: 80%;" value="' + moduleLeaderEmail + '" readonly></p></div></div>' + '</div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
}


function showEventInfoTab(name, type, date, startTime, endTime, room, tutor, comment) {
	profileTab = "eventInfo";
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;">' + '<div class="row"><div class="col-md-2"><p>Name: <input type="text" style="width: 100%;" value="' + name + '" readonly></p></div><div class="col-md-3"><p>Type: <input type="text" style="width: 90%;" value="' + type + '" readonly></p></div><div class="col-md-7"><p>Date: <input type="text" style="width: 100%;" value="' + date.substring(3, 6) + date.substring(0, 3) + date.substring(6, 10) + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-12"><p>Time: <input type="text" style="width: 80%;" value="' + startTime + ' - ' + endTime + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-4"><p>Room: <input type="text" style="width: 100%;" value="' + room + '" readonly></p></div><div class="col-md-8"><p>Tutor: <input type="text" style="width: 80%;" value="' + tutor + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-4"><p>Comment: <input type="text" style="width: 100%;" value="' + comment + '" readonly></p></div></div>' + '</div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
}

function selectImageFromModuleSchool(sch) {
	switch(sch) {
		case "AD":
			return "images/module_artDesign.png";
			break;
		case "BS":
			return "images/module_business.png";
			break;
		case "HE":
			return "images/module_healthEducation.png";
			break;
		case "LW":
			return "images/module_law.png";
			break;
		case "MP":
			return "images/module_mediaPerformingArts.png";
			break;
		case "ST":
			return "images/module_scienceTechnology.png";
			break;
	}
}

function loadModuleLongProfile(dataObj, obj) {
	obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("button")[0].disabled = false;
	
	//alert(JSON.stringify(dataObj));
	
	obj.parentElement.parentElement.parentElement.parentElement.parentElement.innerHTML += '<div class="row" id="longProfileShowHide" style="padding: 0px 5px 5px 5px; font-size: 16px; height: 300px;"><div class="col-md-12"><div class="row" style="border-bottom: solid 2px black; border-top: solid 2px black;"><button onclick="showModuleTab(\''+ dataObj.crn + '\',\'' + dataObj.moduleCode + '\',\'' + dataObj.moduleShortTitle + '\',\'' + dataObj.moduleLongTitle + '\',\'' + dataObj.moduleLeaderName + '\',\'' + dataObj.moduleLeaderEmail.replace(/\r/g, "") + 
	'\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Module</button><button onclick=\'showModuleSLAsTab("' + dataObj.crn + '",' + JSON.stringify(dataObj.moduleSLAs) + ')\' class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">SLAs</button></div><br><div class="row" id="longProfile" style="margin: 0px 0px 0px 0px; padding: 5px 5px 5px 5px; width: 100%; height: 250px; overflow: scroll;"></div></div>';
	
	showModuleTab(dataObj.crn, dataObj.moduleCode, dataObj.moduleShortTitle, dataObj.moduleLongTitle, dataObj.moduleLeaderName, dataObj.moduleLeaderEmail);
}

function loadEventLongProfile(dataObj, obj) {
	obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("button")[0].disabled = false;
	
	//alert(JSON.stringify(dataObj));
	
	obj.parentElement.parentElement.parentElement.parentElement.parentElement.innerHTML += '<div class="row" id="longProfileShowHide" style="padding: 0px 5px 5px 5px; font-size: 16px; height: 300px;"><div class="col-md-12"><div class="row" style="border-bottom: solid 2px black; border-top: solid 2px black;"><button onclick="showEventInfoTab(\''+ dataObj.name + '\',\'' + dataObj.type + '\',\'' + dataObj.date + '\',\'' + dataObj.startTime + '\',\'' + dataObj.endTime + '\',\'' + dataObj.room + '\',\'' + dataObj.tutor +'\',\'' + dataObj.comment + 
	'\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Event</button><button onclick=\'showEventSLAsTab("' + dataObj.eventID + '",' + JSON.stringify(dataObj.eventSLAs) + ')\' class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">SLAs</button></div><br><div class="row" id="longProfile" style="margin: 0px 0px 0px 0px; padding: 5px 5px 5px 5px; width: 100%; height: 250px; overflow: scroll;"></div></div>';
	
	showEventInfoTab(dataObj.name, dataObj.type, dataObj.date, dataObj.startTime, dataObj.endTime, dataObj.room, dataObj.tutor, dataObj.comment);
}



var profileTab = "";

function loadSLALongProfile(dataObj, obj) {
	obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("button")[0].disabled = false;
	
	//alert(JSON.stringify(dataObj));
	//alert(JSON.stringify(dataObj.slaModules));
	
	obj.parentElement.parentElement.parentElement.parentElement.parentElement.innerHTML += '<div class="row" id="longProfileShowHide" style="padding: 0px 5px 5px 5px; font-size: 16px; height: 300px;"><div class="col-md-12"><div class="row" style="border-bottom: solid 2px black; border-top: solid 2px black;"><button onclick="showProfile(\''+ dataObj.title + '\',\'' + dataObj.firstName + '\',\'' + dataObj.lastName + '\',\'' + dataObj.studentNumber + '\',\'' + dataObj.dateOfBirth + '\',\'' + dataObj.uniEmail + '\',\'' + dataObj.personalEmail + '\',\'' + dataObj.mobileNumber + '\',\'' + dataObj.status + '\',\'' + dataObj.withdrawn + '\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Profile</button><button onclick=\'showEducation("' + dataObj.studentNumber + '","' + dataObj.programme + '","' + dataObj.year + '","' + dataObj.school + '",' + JSON.stringify(dataObj.slaModules) + ')\' class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Education</button><button onclick="" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Timetable</button><button onclick=\'showMahara("' + dataObj.studentNumber + '",' + JSON.stringify(dataObj.maharaAssignment) + ',' + JSON.stringify(dataObj.maharaComments) + ')\' class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Mahara</button><button onclick=\'showEventsTab("' + dataObj.studentNumber+ '",' + JSON.stringify(dataObj.slaEvents) + ')\' class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Events</button><button onclick="showApplication(\'' + dataObj.whySLA.replace(/"/g, "") + '\',\'' + dataObj.successFormula.replace(/"/g, "") + '\',\'' + dataObj.previousModule1 + '\',\'' + dataObj.previousModule2 + '\',\'' + dataObj.previousModule3 + '\',\'' + dataObj.previousModule4 +'\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Application</button><button onclick="showSkills(\'' + dataObj.ITSkills.replace(/"/g, "") + '\',\'' + dataObj.otherSkills.replace(/"/g, "") + '\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Skills</button><button onclick="showReference(\'' + dataObj.studentNumber + '\',\'' + dataObj.firstName + '\',\'' + dataObj.lastName + '\',\'' + dataObj.startDate + '\',\'' + dataObj.endDate + '\',\'' + dataObj.comment + '\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Reference</button><button onclick="showOtherTab(\'' + dataObj.disabilityState + '\',\'' + dataObj.disabilityNature + '\',\'Franco Raimondi\', \'CSD1000\')" class="btn btn-info btn-sm" style="margin: 0px 3px 0px 1px; height: 25px; font-size: 14px;">Other</button></div><br><div class="row" id="longProfile" style="margin: 0px 0px 0px 0px; padding: 5px 5px 5px 5px; width: 100%; height: 250px; overflow: scroll;"></div></div>';
	
	showProfile(dataObj.title, dataObj.firstName, dataObj.lastName, dataObj.studentNumber, dataObj.dateOfBirth, dataObj.uniEmail, dataObj.personalEmail, dataObj.mobileNumber, dataObj.status, dataObj.withdrawn);
}


function showProfile(title, firstName, lastName, studentNumber, date, uniEmail, personalEmail, mobileNumber, stat, withdrawn) {
	profileTab = "profile";
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;">' + '<div class="row"><div class="col-md-2"><p>Title: <input type="text" style="width: 80%;" value="' + title + '" readonly></p></div><div class="col-md-5"><p>First Name: <input type="text" style="width: 80%;" value="' + firstName + '" readonly></p></div><div class="col-md-5"><p>Last Name: <input type="text" style="width: 80%;" value="' + lastName + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-12"><p>Date of Birth: <input type="text" style="width: 50%;" value="' + date + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-4"><p>Student Number: <input type="text" style="width: 80%;" value="' + studentNumber + '" readonly></p></div><div class="col-md-8"><p>University Email: <input type="text" style="width: 80%;" value="' + uniEmail + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-5"><p>Mobile: <input type="text" style="width: 80%;" value="' + mobileNumber + '" readonly></p></div><div class="col-md-7"><p>Personal Email: <input type="text" style="width: 80%;" value="' + personalEmail + '" readonly></p></div></div>' + '<div class="row"><p>Status: <input type="text" style="width: 80%;" value="' + checkStatus(stat, withdrawn) + '" readonly></p></div>' + '</div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
	
}

function showEventsTab(studentNumber, eventArr) {
	//alert(eventArr.length);
	//alert(JSON.stringify(eventArr));
	profileTab = "events";
	
	var htmlStr = '<div class="col-md-6"><div class="row"><p>Training</p></div><div class="row">'; 
	
	//training event(s) goes here
	var allTraining = findTrainingEvent(eventArr);
	if(allTraining.length > 0) {
		for(var i=0; i<allTraining.length; i++) {
			htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ccccff22, #0000ff22); margin: 5px 5px 5px -5px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><div class="col-md-5"><p style="margin-bottom: 5px;">' + allTraining[i].name + '</p></div><div class="col-md-7" style="text-align: right;"><button class="deleteButton" onclick="deleteSlaEventProfile(\'' + studentNumber + '\',\'' + allTraining[i].eventID + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></div></div><div class="row"><div class="col-md-12"><p style="margin-bottom: 5px;">' + allTraining[i].date.substring(3, 6) + allTraining[i].date.substring(0, 3) + allTraining[i].date.substring(6, 10) + '</p></div></div><div class="row"><div class="col-md-12"><p style="margin-bottom: 5px;">' + allTraining[i].startTime + ' - ' + allTraining[i].endTime + '</p></div></div></div>';
		}
	} else {
		htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ccccff22, #0000ff22); margin: 5px 5px 5px -5px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><p style="margin-bottom: 5px;">NO TRAINING AVAILABLE.</p></div></div>';
	}
	
	
	htmlStr += '</div></div>' + '<div class="col-md-6" style="border-left: solid 1px black;"><div class="row"><p style="padding-left: 10px;">Other</p></div><div class="row">'; 
	
	//other events go here
	if(eventArr.length > 0) {
		for(var j=0; j<eventArr.length; j++) {
			if(eventArr[j].type != "Training") {
				htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ffeecc22, #ff660022); margin: 5px 5px 5px 5px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><div class="col-md-5"><p style="margin-bottom: 5px;">' + eventArr[j].name + '</p></div><div class="col-md-7" style="text-align: right;"><button class="deleteButton" onclick="deleteSlaEventProfile(\'' + studentNumber + '\',\'' + eventArr[j].eventID + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></div></div><div class="row"><div class="col-md-12"><p style="margin-bottom: 5px;">' + eventArr[j].date.substring(3, 6) + eventArr[j].date.substring(0, 3) + eventArr[j].date.substring(6, 10) + '</p></div></div><div class="row"><div class="col-md-12"><p style="margin-bottom: 5px;">' + eventArr[j].startTime + ' - ' + eventArr[j].endTime + '</p></div></div></div>';
			}
		}
	} else {
		htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ffeecc22, #ff660022); margin: 5px 5px 5px 5px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><p style="margin-bottom: 5px;">NO EVENTS AVAILABLE.</p></div></div>';
	}
	
	htmlStr += '</div></div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
	
}


function showModuleSLAsTab(crn, slaArr) {
	//alert(eventArr.length);
	//alert(JSON.stringify(eventArr));
	profileTab = "moduleSLAs";
	
	var htmlStr = '<div class="col-md-12"><div class="row"><p>Module SLAs</p></div><div class="row">'; 
	
	if(slaArr.length > 0) {
		for(var i=0; i<slaArr.length; i++) {
			htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ccccff22, #0000ff22); margin: 5px 5px 5px -5px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><div class="col-md-5"><p style="margin-bottom: 5px;">' + slaArr[i].firstName + ' ' + slaArr[i].lastName + '</p></div><div class="col-md-7" style="text-align: right;"><button class="deleteButton" onclick="deleteSlaModuleProfile(\'' + slaArr[i].studentNumber + '\',\'' + crn + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></div></div></div>';
		}
	} else {
		htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ccccff22, #0000ff22); margin: 5px 5px 5px -5px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><p style="margin-bottom: 5px;">NO SLAs ON THIS MODULE.</p></div></div>';
	}
	
	
	htmlStr += '</div></div>';
	document.getElementById("longProfile").innerHTML = htmlStr;
	
}

function showEventSLAsTab(eventID, slaArr) {
	//alert(eventArr.length);
	//alert(JSON.stringify(eventArr));
	profileTab = "eventSLAs";
	
	var htmlStr = '<div class="col-md-12"><div class="row"><p>Event SLAs</p></div><div class="row">'; 
	
	if(slaArr.length > 0) {
		for(var i=0; i<slaArr.length; i++) {
			htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ccccff22, #0000ff22); margin: 5px 5px 5px -5px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><div class="col-md-5"><p style="margin-bottom: 5px;">' + slaArr[i].firstName + ' ' + slaArr[i].lastName + '</p></div><div class="col-md-7" style="text-align: right;"><button class="deleteButton" onclick="deleteSlaEventProfile(\'' + slaArr[i].studentNumber + '\',\'' + eventID + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></div></div></div>';
		}
	} else {
		htmlStr += '<div class="col-md-12" style="border-radius: 7px; border: solid 1px black; background-image: radial-gradient(#ccccff22, #0000ff22); margin: 5px 5px 5px -5px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;"><div class="row"><p style="margin-bottom: 5px;">NO SLAs ON THIS EVENT.</p></div></div>';
	}
	
	
	htmlStr += '</div></div>';
	document.getElementById("longProfile").innerHTML = htmlStr;
	
}


function findTrainingEvent(eventArr) {
	var returnArr = [];
	
	for(var e=0; e<eventArr.length; e++) {
		if(eventArr[e].type == "Training") {
			returnArr.push(eventArr[e]);
		}
	}
	
	return returnArr;
}

function showEducation(studentNumber, programme, year, school, supportingModules) {
	profileTab = "education";
	
	//alert(supportingModules);
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;">' + '<div class="row"><div class="col-md-7"><p>Programme: <input type="text" style="width: 70%;" value="' + programme + '" readonly></p></div><div class="col-md-5"><p>Year: <input type="text" style="width: 80%;" value="' + year + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-12"><p>School: <input type="text" style="width: 70%;" value="' + school + '" readonly></p></div><br><br>' + '<div class="row" style="border-top: solid 1px black; width: 100%;"><div class="col-md-12"><p>SUPPORTING MODULES</p></div></div>';
	
	//sla modules go here...
	if(supportingModules.length <= 0) {
		htmlStr += '<div class="row" style="width: 100%; border: solid 1px grey; border-radius: 7px; padding: 4px 4px 4px 4px; margin: 0px 5px 5px 5px;"><div class="col-md-5"><p style="margin: 0px 0px 0px 0px;">No Modules Available.</p></div>';
	} else {
		for(var s=0; s<supportingModules.length; s++) {
			htmlStr += '<div class="row" style="width: 100%; border: solid 1px grey; border-radius: 7px; padding: 4px 4px 4px 4px; margin: 0px 5px 5px 5px;">';
		
			htmlStr += '<div class="col-md-5"><p style="margin: 0px 0px 0px 0px;">' + supportingModules[s].crn + ' | ' + supportingModules[s].moduleCode + '</p></div><div class="col-md-7" style="text-align: right;"><button class="deleteButton" onclick="deleteSlaModuleProfile(\'' + studentNumber + '\',\'' + supportingModules[s].crn + '\')" style="border: none; background-color: #00000000; width: 15px; height: 15px;" disabled><img src="images/icon_delete.png"  style="width: 100%; height: 100%;"></button></div>';
		
			htmlStr += '</div>';
		}
	}
	
	
	htmlStr += '</div>'; 
	
	document.getElementById("longProfile").innerHTML = htmlStr;
}


function showMahara(studentID, maharaAssgntArr, maharaCommentArr) {
	profileTab = "mahara";
	var htmlStr = '<div class="col-md-12">' + '<div class="row" style="border-bottom: solid 1px black; margin-bottom: 5px;"><div class="col-md-12"><div class="row"><p style="margin: 0px 0px 5px 0px;"><u>Add a Comment</u></p></div><div class="row"><p style="width:100%; margin: 0px 0px 5px 0px;">&nbspRating: <select id="maharaRatingSelect"><option value="1">0.5</option><option value="2">1.0</option><option value="3">1.5</option><option value="4">2.0</option><option value="5">2.5</option><option value="6">3.0</option><option value="7">3.5</option><option value="8">4.0</option><option value="9">4.5</option><option value="10">5.0</option></select>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSelect Mahara: <select id="maharaSelect">';
	
	if(maharaAssgntArr.length > 0) {
	for(var e=0; e<maharaAssgntArr.length; e++) {
		htmlStr += '<option value="' + maharaAssgntArr[e].maharaAssignmentID + '">' + maharaAssgntArr[e].title + ' | ' + maharaAssgntArr[e].type + '</option>';
	}
	} else {
		htmlStr += '<option value="noSelect">No Options Available</option>';
	}
	
	htmlStr += '</select></p><br><p style="width:100%; margin: 0px 0px 5px 0px;"> Comment: <input type="text" name="maharaComment" id="maharaComment" placeholder="Enter comment here..." style="width: 70%;">&nbsp&nbsp&nbsp&nbsp<button onclick="saveMaharaComment()" class="btn btn-danger btn-sm">Enter</button></p></div></div></div>'; 
	//alert(JSON.stringify(maharaCommentArr));
	for(var c=0; c<maharaCommentArr.length; c++) {
		htmlStr += '<div class="row"><div class="speechbox speechtriangle">' + '<p style="margin: 0px 0px 0px 0px;">User: ' + maharaCommentArr[c].userID + '</p><p style="font-size: 12px; margin: 0px 0px -3px 0px;">' + maharaCommentArr[c].dateTime + '</p><p style="margin: 0px 0px 8px 0px;">'; 
	
		var fullStars = Math.floor(maharaCommentArr[c].rating/2);
		var halfStars = 0;
		if(maharaCommentArr[c].rating%2 != 0) {
			halfStars = 1;
		}
		var emptyStars = 5 - (fullStars + halfStars);
	
		//full star
		for(var r=0; r<fullStars; r++) {
			htmlStr += '<img src="images/rating_fullStar.png" style="width: 18px;">';
		}
	
		//half star
		for(var r2=0; r2<halfStars; r2++) {
			htmlStr += '<img src="images/rating_halfStar.png" style="width: 18px;">';
		}
	
		//empty star
		for(var r3=0; r3<emptyStars; r3++) {
			htmlStr += '<img src="images/rating_emptyStar.png" style="width: 18px;">';
		}
	
		htmlStr += '</p><p style="margin: 0px 0px 0px 0px; padding-left: 30px; text-indent: -30px;">' + maharaCommentArr[c].title + ', ' + maharaCommentArr[c].type + ': ' + maharaCommentArr[c].comment + '</p>' + '</div></div>'
	}
	
	htmlStr += '</div>'; 
		
	document.getElementById("longProfile").innerHTML = htmlStr;
}


//TO_ADD: modules supported
function showReference(studentNumber, firstName, lastName, startDate, endDate, comment) {
	profileTab = "reference";
	
	//if start date, but no end date, still working
	if(startDate != "null" && endDate == "null") {
		endDate = "Present";
	}
	//if no start date, not yet working
	if(startDate == "null") {
		startDate = "Not Active";
		endDate = "Not Active";
	}
	
	var htmlStr = '<div class="col-md-12">' + '<div class="row"><div class="col-md-12"><p>Student Number: <span>' + studentNumber + '</span></p></div></div>' + '<div class="row"><div class="col-md-12"><p>Name: <span>' + firstName + ' ' + lastName + '</span></p></div></div>' + '<div class="row"><div class="col-md-6"><p>Start Date: <input type="text" style="width: 70%;" value="' + startDate + '" readonly></p></div><div class="col-md-6"><p>End Date: <input type="text" style="width: 70%;" value="' + endDate + '" readonly></p></div></div>' + '<div class="row"><div class="col-md-12"><p>Comments: <textarea style="width:90%; height: 80px;" readonly>' + comment +'</textarea></p></div></div>' + '</div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
}

function showApplication(whySLA, successFormula, previousModule1, previousModule2, previousModule3, previousModule4) {
	profileTab = "application";
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;">' + '<div class="row"><div class="col-md-12"><p>Why would you like to be an SLA?: <textarea style="width:90%; height: 80px;" readonly>' + whySLA +'</textarea></p></div></div>' + '<div class="row"><div class="col-md-12"><p>Success Formula: <textarea style="width:90%; height: 80px;" readonly>' + successFormula + '</textarea></p></div></div>' + '<div class="row" style="border-top: solid 1px black;"><p>Previous Modules</p></div><div class="row"><p><input type="text" style="width: 45%;" value="' + previousModule1 + '" readonly> <input type="text" style="width: 45%;" value="' + previousModule2 + '" readonly></p><p><input type="text" style="width: 45%;" value="' + previousModule3 + '" readonly> <input type="text" style="width: 45%;" value="' + previousModule4 + '" readonly></p></div>' + '</div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
}

function showSkills(itSkills, otherSkills) {
	profileTab = "skills";
	
	itSkills = itSkills.split("|");
	otherSkills = otherSkills.split("|");
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;"><div class="row"><div class="col-md-12"><div class="row"><p style="margin: 2px 2px 2px 2px;">IT Skills</p></div><div class="row">';
	
	var color = "rgba(255,0,0,0.5)";
	for(var i=0; i<itSkills.length; i++) {
		htmlStr += '<div class="col-md-auto" style="background-color:' + color + ';  border-radius: 7px; padding: 4px 4px 4px 4px; margin: 0px 5px 5px 5px;">' + itSkills[i] + '</div>';
		//alert(color);
		color = changeColour(color);
	}
	
	htmlStr += '</div></div></div><div class="row" style="margin-top:20px; border-top: solid 1px black;"><div class="col-md-12"><div class="row"><p style="margin: 2px 2px 2px 2px;">Other Skills</p></div><div class="row">';
	
	for(var o=0; o<otherSkills.length; o++) {
		htmlStr += '<div class="col-md-auto" style="background-color:' + color + ';  border-radius: 7px; padding: 4px 4px 4px 4px; margin: 0px 5px 5px 5px;">' + otherSkills[o] + '</div>';
		//alert(color);
		color = changeColour(color);
	}
	
	htmlStr += '</div></div></div></div>';
	document.getElementById("longProfile").innerHTML = htmlStr;
}

function changeColour(str) {
	switch(str) {
		case "rgba(255,0,0,0.5)":
			return "rgba(255,63,0,0.5)";
		case "rgba(255,63,0,0.5)":
			return "rgba(255,127,0,0.5)";
		case "rgba(255,127,0,0.5)":
			return "rgba(255,191,0,0.5)";
		case "rgba(255,191,0,0.5)":
			return "rgba(255,255,0,0.5)";
		case "rgba(255,255,0,0.5)":
			return "rgba(191,255,0,0.5)";
		case "rgba(191,255,0,0.5)":
			return "rgba(127,255,0,0.5)";
		case "rgba(127,255,0,0.5)":
			return "rgba(63,255,0,0.5)";
		case "rgba(63,255,0,0.5)":
			return "rgba(0,255,0,0.5)";
		case "rgba(0,255,0,0.5)":
			return "rgba(0,255,63,0.5)";
		case "rgba(0,255,63,0.5)":
			return "rgba(0,255,127,0.5)";
		case "rgba(0,255,127,0.5)":
			return "rgba(0,255,191,0.5)";
		case "rgba(0,255,191,0.5)":
			return "rgba(0,255,255,0.5)";
		case "rgba(0,255,255,0.5)":
			return "rgba(0,191,255,0.5)";
		case "rgba(0,191,255,0.5)":
			return "rgba(0,127,255,0.5)";
		case "rgba(0,127,255,0.5)":
			return "rgba(0,63,255,0.5)";
		case "rgba(0,63,255,0.5)":
			return "rgba(0,0,255,0.5)";
		case "rgba(0,0,255,0.5)":
			return "rgba(63,0,255,0.5)";
		case "rgba(63,0,255,0.5)":
			return "rgba(127,0,255,0.5)";
		case "rgba(127,0,255,0.5)":
			return "rgba(191,0,255,0.5)";
		case "rgba(191,0,255,0.5)":
			return "rgba(255,0,255,0.5)";
		case "rgba(255,0,255,0.5)":
			return "rgba(255,0,191,0.5)";
		case "rgba(255,0,191,0.5)":
			return "rgba(255,0,127,0.5)";
		case "rgba(255,0,127,0.5)":
			return "rgba(255,0,63,0.5)";
		case "rgba(255,0,63,0.5)":
			return "rgba(255,0,0,0.5)";
		default:
			return "rgba(255,0,0,0.5)";
	}
  }

function showOtherTab(disabilityState, disabilityNature, nominationLecturer, nominationModule1) {
	profileTab = "other";
	
	var htmlStr = '<div class="col-md-12" style="padding: 10px 10px 10px 10px; margin: 0px 0px 0px 0px;">' + '<div class="row"><div class="col-md-12"><p>Disability: <input type="text" style="width: 20%;" value="' + disabilityState + '" readonly> <input type="text" style="width: 50%;" value="' + disabilityNature + '" readonly></p></div></div>' + '<div class="row" style="border-top: solid 1px black;"><p>[NL] Nomination Details</p></div><div class="row"><p>Lecturer: <input type="text" style="width: 80%;" value="' + nominationLecturer + '" readonly></p><p>Module(s): <input type="text" style="width: 80%;" value="' + nominationModule1 + '" readonly></p></div>' + '</div>';
	
	document.getElementById("longProfile").innerHTML = htmlStr;
}

function editSLA(studentNumber, obj) {
	obj.innerHTML = '<img src="images/icon_save.png" style="width: 100%; height: 100%;">';
	obj.setAttribute('onclick', 'saveSLA(\''+ studentNumber +'\', this)');
	
	var allInput = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input");
	var allTextArea = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("textarea");
	var allDeleteButtons = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll(".deleteButton");
	
	for(var c=0; c<allInput.length; c++) {
		allInput[c].readOnly = false;
	}
	for(var t=0; t<allTextArea.length; t++) {
		allTextArea[t].readOnly = false;
	}
	for(var d=0; d<allDeleteButtons.length; d++) {
		allDeleteButtons[d].disabled = false;
	}
}

function editModule(crn, obj) {
	obj.innerHTML = '<img src="images/icon_save.png" style="width: 100%; height: 100%;">';
	obj.setAttribute('onclick', 'saveModule(\''+ crn +'\', this)');
	
	var allInput = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input");
	var allTextArea = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("textarea");
	var allDeleteButtons = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll(".deleteButton");
	
	for(var c=0; c<allInput.length; c++) {
		allInput[c].readOnly = false;
	}
	for(var t=0; t<allTextArea.length; t++) {
		allTextArea[t].readOnly = false;
	}
	for(var d=0; d<allDeleteButtons.length; d++) {
		allDeleteButtons[d].disabled = false;
	}
}

function editEvent(eventID, obj) {
	obj.innerHTML = '<img src="images/icon_save.png" style="width: 100%; height: 100%;">';
	obj.setAttribute('onclick', 'saveEvent(\''+ eventID +'\', this)');
	
	var allInput = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input");
	var allTextArea = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("textarea");
	var allDeleteButtons = obj.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll(".deleteButton");
	
	for(var c=0; c<allInput.length; c++) {
		allInput[c].readOnly = false;
	}
	for(var t=0; t<allTextArea.length; t++) {
		allTextArea[t].readOnly = false;
	}
	for(var d=0; d<allDeleteButtons.length; d++) {
		allDeleteButtons[d].disabled = false;
	}
}


function maximize(dataObj, obj, clsf) {
	//alert(JSON.stringify(dataObj));
	
	//if maximized, minimize
	if(obj.innerHTML == '<img src="images/icon_minimize.png" style="width: 100%; height: 100%;">') {
		var colretSize = 0;
		var colretFinal = 0;
		if(obj.parentElement.parentElement.parentElement.parentElement.parentElement.id == "shortProfileGrid") {
			colretSize = 12;
			colretFinal = 5;
		} else {
			colretSize = 12;
			colretFinal = 12;
		}
		var idIn = setInterval(frameIn, 10);
		function frameIn() {
			if (colretSize < colretFinal) {
				clearInterval(idIn);
				obj.parentElement.parentElement.parentElement.parentElement.parentElement.removeChild(document.getElementById("longProfileShowHide"));
				obj.innerHTML = '<img src="images/icon_maximize.png" style="width: 100%; height: 100%;">';
			} else {
				//alert("min " + colretSize);
				obj.parentElement.parentElement.parentElement.parentElement.parentElement.className="col-md-" + colretSize;
				colretSize--;
			}
		}
	} else { //else maximize
		var colSize = 5;
		var colFinal
		if(obj.parentElement.parentElement.parentElement.parentElement.parentElement.id == "shortProfileGrid") {
			colSize = 5;
			colFinal = 12;
		} else {
			colSize = 12;
			colFinal = 12;
		}
		
		var idOut = setInterval(frameOut, 10);
		function frameOut() {
			if (colSize > colFinal) {
				clearInterval(idOut);
				removeAllLongProfile();
				obj.innerHTML = '<img src="images/icon_minimize.png"  style="width: 100%; height: 100%;">';
				if(clsf == "sla") {
					loadSLALongProfile(dataObj, obj);
				} else if(clsf == "module") {
					loadModuleLongProfile(dataObj, obj);
				} else if(clsf == "event") {
					loadEventLongProfile(dataObj, obj);
				}
			} else {
				//alert("max " + colSize);
				obj.parentElement.parentElement.parentElement.parentElement.parentElement.className="col-md-" + colSize;
				colSize++;
			}
		}
	}
	
}

function removeAllLongProfile() {
	var change = document.querySelectorAll("#longProfileShowHide");
	for(var r=0; r<change.length; r++) {
		if(change[r].parentElement.id == "shortProfileGrid") {
			change[r].parentElement.className = "col-md-5";
		} else {
			document.querySelectorAll("#longProfileShowHide")[r].parentElement.className = "col-md-12";
		}
		change[r].parentElement.removeChild(change[r]);
	}
	
	//make all maximize
	change = document.querySelectorAll(".minMaxButton");
	for(var b=0; b<change.length; b++) {
		change[b].childNodes[0].src = "images/icon_maximize.png";
	}
	
	
}

function checkStatus(slaStat, slaWithdrawn) {
	if(slaWithdrawn == "Yes") {
		return "Withdrawn";
	} else {
		return slaStat;
	}
}


function school2Code(skl) {
	switch(skl) {
		case "Art & Design":
			return "AD";
			break;
		case "Business":
			return "BS";
			break;
		case "Health & Education":
			return "HE";
			break;
		case "Law":
			return "LW";
			break;
		case "Media & Performing Arts":
			return "MP";
			break;
		case "Science & Technology":
			return "ST";
			break;
		default:
			return "NA";
			break;
	}
}

function selectShortProfileImage(slaObj) {
	if(slaObj.seniorSLA == "Yes") {
		return "images/sslaProfile.png";
	} else {
		return "images/slaProfile.png";
	}
} 

function status3Code(ret, sen) {
	if(sen == "Yes") {
		return "SEN";
	} else if(ret == "Yes") {
		return "RET";
	} else {
		return "NEW";
	}
}

function clearSearch() {
	location.reload();
} 
  
function sortEvents(eArray) {
	//sort array
	eArray.sort(function(a, b){return new Date(a.date + " " + a.startTime).getTime() - new Date(b.date + " " + b.startTime).getTime()});
	
	//alert(JSON.stringify(eArray));
	
	var date1 = new Date();
	//change to short date
	for(var i=0; i<eArray.length; i++) {
		if(new Date(eArray[i].date + " " + eArray[i].endTime) < date1) {
			eArray.splice(i, i+1);
			//continue; 
			i-=1;
		}
	}
	
	//check closest event
	var todayEvent = ["no event"];
	
	if(eArray.length > 0) {
	var date2 = new Date(eArray[0].date);
	var dateDiff = (date2.getDate() - date1.getDate()) + 
		(30 * (date2.getMonth() - date1.getMonth())) + (365 * (date2.getFullYear() - date1.getFullYear()));
	
	if(dateDiff == 0) {
		todayEvent[0] = eArray[0];
		eArray.splice(0, 1);
		//alert(dateDiff);
		todayEvent[0].date = new Date(todayEvent[0].date).toLocaleDateString();
	}
	
	for(var i=0; i<eArray.length; i++) {
		eArray[i].date = new Date(eArray[i].date).toLocaleDateString();
	}
	}
	
	todayEvent[1] = eArray;
	return todayEvent;
}

function updateEvents(eventArr) {
	//alert(JSON.stringify(eventArr));
	eventArr = sortEvents(eventArr);
	//alert(JSON.stringify(eventArr));
	var tName = document.getElementById("todayEventName");
	var tDate = document.getElementById("todayEventDate");
	var tTime = document.getElementById("todayEventTime");
	var tIcon = document.getElementById("todayEventIcon");
	var tButton = document.getElementById("todayEventButton");
	
	var nName = document.getElementById("nextEventName");
	var nDate = document.getElementById("nextEventDate");
	var nTime = document.getElementById("nextEventTime");
	var nIcon = document.getElementById("nextEventIcon");
	var nButton = document.getElementById("nextEventButton");
	
	if(eventArr[0] == "no event") {
		tName.innerHTML = "No events today.";
		tIcon.src = "images/icon_noEvent.png";
		tButton.innerHTML = "View All Events [NL]";
	} else {
		tName.innerHTML = eventArr[0].name;
		tDate.innerHTML = eventArr[0].date;
		tTime.innerHTML = eventArr[0].startTime + " - " + eventArr[0].endTime;
		tIcon.src = "images/icon_event.png";
		tButton.innerHTML = "View Event [NL]";
	}
	
	if(eventArr[1].length > 0) {
		nName.innerHTML = eventArr[1][0].name;
		nDate.innerHTML = eventArr[1][0].date;
		nTime.innerHTML = eventArr[1][0].startTime + " - " + eventArr[1][0].endTime;
		nIcon.src = "images/icon_event.png";
		nButton.innerHTML = "View Event [NL]";
	} else {
		nName.innerHTML = "No events coming up.";
		nIcon.src = "images/icon_noEvent.png";
		nButton.innerHTML = "View All Events [NL]";
	}
}  
  
function formatDate() {
	var dateObj = new Date();
	var dateStr = days[dateObj.getDay()].substring(0, 3) + ", " + numberFormat(dateObj.getDate()) + "/" + numberFormat(dateObj.getMonth() + 1) + "/" + dateObj.getFullYear().toString().substring(2, 4) + " " + numberFormat(dateObj.getHours()) + ":" + numberFormat(dateObj.getMinutes());
	
	return dateStr;
}

function uploadSLAFile() {
  var fileUpload = document.getElementById("fileUpload");
  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
  if (regex.test(fileUpload.value.toLowerCase())) {
    if (typeof (FileReader) != "undefined") {      
      var reader = new FileReader();
      reader.onload = function (e) {
        var rows = e.target.result.split("\n");
        for (var i = 1; i < rows.length; i++) {
			if(rows[i] != "") {
          var cells = rows[i].replace(/,/g , "<3");
	  
			//alert(rows[i]);
	  
          cells = cells.replace(/<3 /g, ", ");
          cells = cells.split("<3");
          var sla = {};
             sla.title = cells[2];
             sla.firstName = cells[3];
             sla.lastName = cells[4];
			 
			 sla.dateOfBirth = new Date(cells[5].substring(3,6) + cells[5].substring(0, 3) + cells[5].substring(6, 10)).toLocaleDateString();
			 
             sla.studentNumber = cells[6];
             sla.uniEmail = cells[7];
             sla.personalEmail = cells[8];
             sla.mobileNumber = cells[9];
             sla.disabilityState = cells[10];
             sla.disabilityNature = cells[11];
             sla.programme = cells[12];
             sla.year = cells[13];
             sla.school = cells[14];
             sla.foundation = cells[15];
             sla.itSkills = cells[16];
             sla.otherSkills = cells[17];
             sla.returningSLA = cells[18];
             sla.previousModule1 = cells[19];
             sla.previousModule2 = cells[20];
             sla.previousModule3 = cells[21];
             sla.previousModule4 = cells[22]; 
			 sla.whySLA = cells[23].replace(/'/g, "");
             sla.successFormula = cells[24].replace(/'/g, "");
			 
             slaArray[i-1] = sla;
        }
		}
        uploadSLAs();
      }
      reader.readAsText(fileUpload.files[0]);
    } else {
      alert("This browser does not support HTML5.");
    }
  } else {
  alert("Please upload a valid CSV file.");
  }
}

function splitCSVButIgnoreCommasInDoublequotes(str) {  
    //split the str first  
    //then merge the elments between two double quotes  
    var delimiter = ',';  
    var quotes = '"';  
    var elements = str.split(delimiter);  
    var newElements = [];  
    for (var i = 0; i < elements.length; ++i) {  
        if (elements[i].indexOf(quotes) >= 0) {//the left double quotes is found  
            var indexOfRightQuotes = -1;  
            var tmp = elements[i];  
            //find the right double quotes  
            for (var j = i + 1; j < elements.length; ++j) {  
                if (elements[j].indexOf(quotes) >= 0) {  
                    indexOfRightQuotes = j; 
                    break;
                }  
            }  
            //found the right double quotes  
            //merge all the elements between double quotes  
            if (-1 != indexOfRightQuotes) {   
                for (var j = i + 1; j <= indexOfRightQuotes; ++j) {  
                    tmp = tmp + delimiter + elements[j];  
                }  
                newElements.push(tmp);  
                i = indexOfRightQuotes;  
            }  
            else { //right double quotes is not found  
                newElements.push(elements[i]);  
            }  
        }  
        else {//no left double quotes is found  
            newElements.push(elements[i]);  
        }  
    }  
	 return newElements;  
}  

function uploadModuleFile() {
  var fileUpload = document.getElementById("fileUpload");
  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
  if (regex.test(fileUpload.value.toLowerCase())) {
    if (typeof (FileReader) != "undefined") {      
      var reader = new FileReader();
      reader.onload = function (e) {
        var rows = e.target.result.split("\n");
		
		//alert(rows.length);
		
		var empty = 0;
        for (var i = 1; i < rows.length; i++) {
			if(rows[i] != "") {
          // var cells = rows[i].replace(/,/g , "<3");
	  
			// alert(rows[i]);
	  
          // cells = cells.replace(/<3 /g, ", ");
		  // cells = cells.replace(/'/g, "");
		  // //cells = cells.replace(/"/g, "");
		  
		  // var arr = cells.match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g);
		  
		  // alert(arr);
		  
		  
          // cells = cells.split("<3");
		  
		  var cells = splitCSVButIgnoreCommasInDoublequotes(rows[i].replace(/'/g, ""));
		  
          var module = {};
             module.schoolCode = cells[1];
             module.moduleCode = cells[2];
             module.moduleShortTitle = cells[3];
			 module.moduleLongTitle = cells[4];
			 module.crn = cells[6];
             module.moduleLeaderName = cells[9];
             module.moduleLeaderEmail = cells[10];
             
             moduleArray[empty] = module;
			 empty++;
			 
			 //alert(JSON.stringify(module));
			 
			 if(moduleArray.length == 100) {
				 uploadModules();
				 moduleArray = [];
				 empty = 0;
			 }
        }
		}
        uploadModules();
      }
      reader.readAsText(fileUpload.files[0]);
    } else {
      alert("This browser does not support HTML5.");
    }
  } else {
  alert("Please upload a valid CSV file.");
  }
}


function findMonth(number) {
  if(number == "1") {
    return "January";
  }
  else if(number == "2") {
    return "February";
  }
  else if(number == "3") {
    return "March";
  }
  else if(number == "4") {
    return "April";
  }
  else if(number == "5") {
    return "May";
  }
  else if(number == "6") {
    return "June";
  }
  else if(number == "7") {
    return "July";
  }
  else if(number == "8") {
    return "August";
  }
  else if(number == "9") {
    return "September";
  }
  else if(number == "10") {
    return "October";
  }
  else if(number == "11") {
    return "November";
  }
  else {
    return "December";
  }
}


function numberFormat(number){
  if(number == "1"){
    return "01";
  }
  else if(number == "2"){
    return "02";
  }
  else if(number == "3"){
    return "03";
  }
  else if(number == "4"){
    return "04";
  }
  else if(number == "5"){
    return "05";
  }
  else if(number == "6"){
    return "06";
  }
  else if(number == "7"){
    return "07";
  }
  else if(number == "8"){
    return "08";
  }
  else if(number == "9"){
    return "09";
  }
  else {
    return number;
  }
}